<?php
/**
 * Options page functions.
 *
 * @author  TeamWP @Potenza Global Solutions
 * @package car-dealer-helper
 */

/**
 * Admin alert notice functions.
 *
 * @param string  $msg      Notice message.
 * @param string  $status   Notice status.
 * @param string  $heading  Notice heading.
 * @param boolean $return   Whether to return content.
 * @return mixed
 */
function cdhl_admin_notice( $msg = '', $status = 'error', $heading = '', $return = true ) {
	$heading = ( ! empty( $heading ) && is_string( $heading ) ) ? $heading : ucfirst( $status );

	if ( $return ) {
		ob_start();
	}
	?>
	<div class="notice notice-<?php echo esc_attr( $status ); ?> cdhl-is-dismissible fade">
		<p style="line-height: 150%">
			<strong><?php echo esc_html( $heading ); ?></strong></br>
			<?php
				echo wp_kses(
					$msg,
					array(
						'br' => array(),
					)
				);
			?>
		</p>
		<button type="button" class="notice-dismiss"><span class="screen-reader-text"><?php echo esc_html__( 'Dismiss this notice', 'cardealer-helper' ); ?></span></button>
	</div>
	<?php
	if ( $return ) {
		return ob_get_clean();
	}
}

/**
 * Return array of additional attributes.
 *
 * @return array
 */
function cdhl_get_additional_attributes() {
	$cdhl_additional_attributes = get_option( 'cdhl_additional_attributes' );

	return $cdhl_additional_attributes;
}

/**
 * Return html of additional attributes.
 *
 * @return void
 */
function cdhl_get_additional_attributes_html() {
	$additional_attributes = cdhl_get_additional_attributes();

	if ( ! empty( $additional_attributes ) ) {
		$i = 1;
		foreach ( $additional_attributes as $key => $attr ) {
			?>
			<tr class="row-<?php echo esc_attr( $i ); ?>">
				<td><?php echo esc_html( $attr['singular_name'] ); ?></td>
				<td><?php echo esc_html( $attr['plural_name'] ); ?></td>
				<td><?php echo esc_html( $attr['slug'] ); ?></td>
				<td>
					<a data-id="index-<?php echo esc_attr( $i ); ?>" class="edit-attr" href="javascript:void(0);"><span class="dashicons dashicons-edit-large"></span></a>
					<a data-id="index-<?php echo esc_attr( $i ); ?>" data-slug="<?php echo esc_html( $attr['slug'] ); ?>" data-alerttxt="<?php echo esc_html__( 'Are you sure?', 'cardealer-helper' ); ?>" class="delete-attr" href="javascript:void(0);"><span class="dashicons dashicons-trash"></span></a>
					<span class="spinner"></span>
				</td>
			</tr>
			<tr class="edit-row" id="index-<?php echo esc_attr( $i ); ?>" style="display:none;">
				<td colspan="4">
					<div class="">
						<label><?php esc_html_e( 'Singular name', 'cardealer-helper' ); ?>:</label>
						<input type="text" name="singular_name" id="singular-name-index-<?php echo esc_attr( $i ); ?>" class="regular-text" value="<?php echo esc_html( $attr['singular_name'] ); ?>">
						<label for=""><?php esc_html_e( 'Plural name', 'cardealer-helper' ); ?>:</label>
						<input type="text" name="plural_name" id="plural-name-index-<?php echo esc_attr( $i ); ?>" class="regular-text" value="<?php echo esc_html( $attr['plural_name'] ); ?>">
						<input type="submit" name="edit-additional-attributes-submit" data-id="index-<?php echo esc_attr( $i ); ?>" data-slug="<?php echo esc_attr( $attr['slug'] ); ?>" class="button edit-additional-attributes-submit" value="Submit">
						<span class="spinner"></span>
					</div>
				</td>
			</tr>
			<?php
			$i++;
		}
	} else {
		?>
		<tr class="row">
			<td colspan="4"><?php echo esc_html__( 'No data found', 'cardealer-helper' ); ?></td>
		</tr>
		<?php
	}
}

/**
 * Add/Edit additional attributes
 */
function cdhl_add_edit_additional_attributes() {
	$response = array(
		'status' => 'error',
		'msg'    => esc_html__( 'Something went wrong!', 'cardealer-helper' ),
	);
	$error    = array();
	$heading  = esc_html__( 'One or more fields have an error. Please check and try again.', 'cardealer-helper' );

	if ( ! isset( $_POST['nonce'] ) ) {
		$response['msg'] = cdhl_admin_notice( $response['msg'], 'error', esc_html__( 'Oops!', 'cardealer-helper' ) );
		wp_send_json( $response );
		wp_die();
	}

	if ( wp_verify_nonce( wp_unslash( $_POST['nonce'] ), 'add_edit_additional_attributes' ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

		if ( ! current_user_can( 'manage_options' ) ) {
			$response['msg'] = cdhl_admin_notice(
				esc_html__( 'Sorry, you are not allowed to create attributes.', 'cardealer-helper' ),
				'error',
				esc_html__( 'You need a higher level of permission.', 'cardealer-helper' )
			);
			wp_send_json( $response );
			wp_die();
		}

		$singular_name  = ( isset( $_POST['singular_name'] ) && ! empty( $_POST['singular_name'] ) ) ? sanitize_text_field( wp_unslash( $_POST['singular_name'] ) ) : '';
		$plural_name    = ( isset( $_POST['plural_name'] ) && ! empty( $_POST['plural_name'] ) ) ? sanitize_text_field( wp_unslash( $_POST['plural_name'] ) ) : '';
		$attribute_slug = ( isset( $_POST['attribute_slug'] ) && ! empty( $_POST['attribute_slug'] ) ) ? sanitize_text_field( wp_unslash( $_POST['attribute_slug'] ) ) : '';
		$attribute_slug = ( ! empty( $attribute_slug ) ) ? sanitize_title( $attribute_slug ) : $attribute_slug;
		$action_mode    = ( isset( $_POST['action'] ) && 'edit_additional_attributes' === $_POST['action'] ) ? 'edit' : 'add';

		if ( empty( $singular_name ) ) {
			$error[] = esc_html__( 'Please enter singular name.', 'cardealer-helper' );
		}

		if ( empty( $plural_name ) ) {
			$error[] = esc_html__( 'Please enter plural name', 'cardealer-helper' );
		}

		if ( 'edit' === $action_mode && empty( $attribute_slug ) ) {
			$error[] = esc_html__( 'Slug is missing', 'cardealer-helper' );
		}

		if ( 'add' === $action_mode && ! empty( $attribute_slug ) && strlen( $attribute_slug ) > 32 ) {
			$error[] = esc_html__( 'Slug must be 32 characters in length.', 'cardealer-helper' );
		}

		if ( ! empty( $error ) ) {
			$msg = implode( '</br>', $error );

			$response['msg'] = cdhl_admin_notice( $msg, 'error', $heading );

			wp_send_json( $response );
			exit();
		}

		$singular_name_slug = sanitize_title( $singular_name );
		$plural_name_slug   = sanitize_title( $plural_name );

		$cdhl_additional_attributes_data = get_option( 'cdhl_additional_attributes' );
		$cdhl_additional_attributes      = array();

		if ( 'add' === $action_mode ) {

			// Get current register taxonomies.
			$cars_taxonomies = get_object_taxonomies( 'cars' );

			if ( ! empty( $attribute_slug ) && in_array( $attribute_slug, $cars_taxonomies, true ) ) {
				$error[] = sprintf(
					/* translators: %s: taxonomy slug */
					esc_html__( 'Attribute "%s" already exists.', 'cardealer-helper' ),
					esc_html( $attribute_slug )
				);
			} elseif ( in_array( $singular_name_slug, $cars_taxonomies, true ) ) {
				$error[] = sprintf(
					/* translators: %s: taxonomy slug */
					esc_html__( 'Attribute "%s" already exists.', 'cardealer-helper' ),
					esc_html( $singular_name_slug )
				);
			} elseif ( in_array( $plural_name_slug, $cars_taxonomies, true ) ) {
				$error[] = sprintf(
					/* translators: %s: taxonomy slug */
					esc_html__( 'Attribute "%s" already exists.', 'cardealer-helper' ),
					esc_html( $plural_name_slug )
				);
			}

			if ( ! empty( $error ) ) {
				$msg             = implode( '</br>', $error );
				$response['msg'] = cdhl_admin_notice( $msg, 'error', $heading );

				wp_send_json( $response );
				exit();
			}

			$new_attributes   = array();
			$new_attributes[] = array(
				'singular_name' => $singular_name,
				'plural_name'   => $plural_name,
				'slug'          => ! empty( $attribute_slug ) ? $attribute_slug : $singular_name_slug,
			);

			if ( ! empty( $cdhl_additional_attributes_data ) && is_array( $cdhl_additional_attributes_data ) ) {
				$cdhl_additional_attributes = array_merge(
					$cdhl_additional_attributes_data,
					$new_attributes
				);
			} else {
				$cdhl_additional_attributes = $new_attributes;
			}

			$response['msg'] = cdhl_admin_notice( esc_html__( 'Attribute added successfully.', 'cardealer-helper' ), 'success' );
			$response['redirect'] = add_query_arg( array(
				'post_type' => 'cars',
				'page'      => 'additional_attributes_page',
				'message'   => 'attribute_added',
			), admin_url( 'edit.php' ) );

		} else {
			if ( ! empty( $cdhl_additional_attributes_data ) && is_array( $cdhl_additional_attributes_data ) ) {
				$old_array = $cdhl_additional_attributes_data;
				foreach ( $old_array as $key => $attr ) {
					if ( $attribute_slug === $attr['slug'] ) {
						$cdhl_additional_attributes_data[ $key ]['singular_name'] = $singular_name;
						$cdhl_additional_attributes_data[ $key ]['plural_name']   = $plural_name;
					}
				}
				$cdhl_additional_attributes = $cdhl_additional_attributes_data;
			}

			$response['msg'] = cdhl_admin_notice( esc_html__( 'Attribute updated successfully.', 'cardealer-helper' ), 'success' );
			$response['redirect'] = add_query_arg( array(
				'post_type' => 'cars',
				'page'      => 'additional_attributes_page',
				'message'   => 'attribute_updated',
			), admin_url( 'edit.php' ) );

		}

		update_option( 'cdhl_additional_attributes', $cdhl_additional_attributes );

		$response['status'] = 'success';

		ob_start();
		cdhl_get_additional_attributes_html();
		$response['data'] = ob_get_clean();
	}
	wp_send_json( $response );
	exit();
}
add_action( 'wp_ajax_add_additional_attributes', 'cdhl_add_edit_additional_attributes' );
add_action( 'wp_ajax_edit_additional_attributes', 'cdhl_add_edit_additional_attributes' );

/**
 * Delete additional attributes
 */
function cdhl_delete_additional_attributes() {
	$response = array(
		'status' => 'error',
		'msg'    => esc_html__( 'Something went wrong!', 'cardealer-helper' ),
	);
	$heading  = esc_html__( 'One or more fields have an error. Please check and try again.', 'cardealer-helper' );
	$error    = array();

	if ( ! isset( $_POST['nonce'] ) ) {
		$response['msg'] = cdhl_admin_notice( $response['msg'], 'error', esc_html__( 'Oops!', 'cardealer-helper' ) );
		wp_send_json( $response );
		wp_die();
	}

	if ( wp_verify_nonce( wp_unslash( $_POST['nonce'] ), 'add_edit_additional_attributes' ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

		$attribute_slug = ( isset( $_POST['attribute_slug'] ) && ! empty( $_POST['attribute_slug'] ) ) ? sanitize_text_field( wp_unslash( $_POST['attribute_slug'] ) ) : '';

		if ( empty( $attribute_slug ) ) {
			$error[] = esc_html__( 'Slug is missing', 'cardealer-helper' );
		}

		if ( ! empty( $error ) ) {
			$msg             = implode( '</br>', $error );
			$response['msg'] = cdhl_admin_notice( $msg, 'error', $heading );
			wp_send_json( $response );
			exit();
		}

		$cdhl_additional_attributes_data = get_option( 'cdhl_additional_attributes' );

		if ( ! empty( $cdhl_additional_attributes_data ) && is_array( $cdhl_additional_attributes_data ) ) {

			$old_array = $cdhl_additional_attributes_data;
			foreach ( $old_array as $key => $eattr ) {
				if ( $attribute_slug === $eattr['slug'] ) {
					unset( $cdhl_additional_attributes_data[ $key ] );
				}
			}

			$cdhl_additional_attributes = array_values( $cdhl_additional_attributes_data );

		}

		update_option( 'cdhl_additional_attributes', $cdhl_additional_attributes );

		$response['status'] = 'success';
		$response['msg']    = cdhl_admin_notice( esc_html__( 'Attribute deleted successfully.', 'cardealer-helper' ), 'success' );
		$response['redirect'] = add_query_arg( array(
			'post_type' => 'cars',
			'page'      => 'additional_attributes_page',
			'message'   => 'attribute_deleted',
		), admin_url( 'edit.php' ) );

		ob_start();
		cdhl_get_additional_attributes_html();
		$response['data'] = ob_get_clean();
	}
	wp_send_json( $response );
	exit();
}
add_action( 'wp_ajax_delete_additional_attributes', 'cdhl_delete_additional_attributes' );

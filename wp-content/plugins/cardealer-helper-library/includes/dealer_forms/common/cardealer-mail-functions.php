<?php
/**
 * Mail functions
 *
 * @package car-dealer-helper
 */

if ( ! function_exists( 'cdhl_get_text_mail_body' ) ) {
	/**
	 * Function For Text Mail Body
	 *
	 * @param int $car_id car id.
	 */
	function cdhl_get_text_mail_body( $car_id ) {
		$product_plain  = '';
		$sale_price     = ( get_field( 'sale_price', $car_id ) ) ? get_field( 'sale_price', $car_id ) : '&nbsp;';
		$product_plain .= esc_html__( 'Sale Price: ', 'cardealer-helper' ) . $sale_price;

		$regular_price  = ( get_field( 'regular_price', $car_id ) ) ? get_field( 'regular_price', $car_id ) : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Regular Price  : ', 'cardealer-helper' ) . $regular_price;

		$car_year       = wp_get_post_terms( $car_id, 'car_year' );
		$car_year       = ( ! is_wp_error( $car_year ) && ! empty( $car_year ) ) ? $car_year[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Year : ', 'cardealer-helper' ) . $car_year;

		$car_make       = wp_get_post_terms( $car_id, 'car_make' );
		$car_make       = ( ! is_wp_error( $car_make ) && ! empty( $car_make ) ) ? $car_make[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Make : ', 'cardealer-helper' ) . $car_make;

		$car_model      = wp_get_post_terms( $car_id, 'car_model' );
		$car_model      = ( ! is_wp_error( $car_model ) && ! empty( $car_model ) ) ? $car_model[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Model : ', 'cardealer-helper' ) . $car_model;

		$car_body_style = wp_get_post_terms( $car_id, 'car_body_style' );
		$car_body_style = ( ! is_wp_error( $car_body_style ) && ! empty( $car_body_style ) ) ? $car_body_style[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle BodyStyle : ', 'cardealer-helper' ) . $car_body_style;

		$car_mileage    = wp_get_post_terms( $car_id, 'car_mileage' );
		$car_mileage    = ( ! is_wp_error( $car_mileage ) && ! empty( $car_mileage ) ) ? $car_mileage[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Mileage : ', 'cardealer-helper' ) . ( ! isset( $car_mileage ) ) ? '&nbsp;' : $car_mileage[0]->name;

		$car_fuel_economy = wp_get_post_terms( $car_id, 'car_fuel_economy' );
		$car_fuel_economy = ( ! is_wp_error( $car_fuel_economy ) && ! empty( $car_fuel_economy ) ) ? $car_fuel_economy[0]->name : '&nbsp;';
		$product_plain   .= esc_html__( 'Vehicle Fuel Economy : ', 'cardealer-helper' ) . $car_fuel_economy;

		$car_transmission = wp_get_post_terms( $car_id, 'car_transmission' );
		$car_transmission = ( ! is_wp_error( $car_transmission ) && ! empty( $car_transmission ) ) ? $car_transmission[0]->name : '&nbsp;';
		$product_plain   .= PHP_EOL . esc_html__( 'Vehicle Transmission : ', 'cardealer-helper' ) . $car_transmission;

		$car_condition  = wp_get_post_terms( $car_id, 'car_condition' );
		$car_condition  = ( ! is_wp_error( $car_condition ) && ! empty( $car_condition ) ) ? $car_condition[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Condition : ', 'cardealer-helper' ) . $car_condition;

		$car_drivetrain = wp_get_post_terms( $car_id, 'car_drivetrain' );
		$car_drivetrain = ( ! is_wp_error( $car_drivetrain ) && ! empty( $car_drivetrain ) ) ? $car_drivetrain[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Drivetrain : ', 'cardealer-helper' ) . $car_drivetrain;

		$car_engine     = wp_get_post_terms( $car_id, 'car_engine' );
		$car_engine     = ( ! is_wp_error( $car_engine ) && ! empty( $car_engine ) ) ? $car_engine[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Engine : ', 'cardealer-helper' ) . $car_engine;

		$car_exterior_color = wp_get_post_terms( $car_id, 'car_exterior_color' );
		$car_exterior_color = ( ! is_wp_error( $car_exterior_color ) && ! empty( $car_exterior_color ) ) ? $car_exterior_color[0]->name : '&nbsp;';
		$product_plain     .= PHP_EOL . esc_html__( 'Vehicle Exterior Color : ', 'cardealer-helper' ) . $car_exterior_color;

		$car_interior_color = wp_get_post_terms( $car_id, 'car_interior_color' );
		$car_interior_color = ( ! is_wp_error( $car_interior_color ) && ! empty( $car_interior_color ) ) ? $car_interior_color[0]->name : '&nbsp;';
		$product_plain     .= PHP_EOL . esc_html__( 'Vehicle Interior Color : ', 'cardealer-helper' ) . $car_interior_color;

		$car_stock_number = wp_get_post_terms( $car_id, 'car_stock_number' );
		$car_stock_number = ( ! is_wp_error( $car_stock_number ) && ! empty( $car_stock_number ) ) ? $car_stock_number[0]->name : '&nbsp;';
		$product_plain   .= PHP_EOL . esc_html__( 'Vehicle Stock Number : ', 'cardealer-helper' ) . $car_stock_number;

		$car_vin_number = wp_get_post_terms( $car_id, 'car_vin_number' );
		$car_vin_number = ( ! is_wp_error( $car_vin_number ) && ! empty( $car_vin_number ) ) ? $car_vin_number[0]->name : '&nbsp;';
		$product_plain .= PHP_EOL . esc_html__( 'Vehicle Vin Number : ', 'cardealer-helper' ) . $car_vin_number;

		$taxonomies_raw = get_object_taxonomies( 'cars' );
		foreach ( $taxonomies_raw as $new_tax ) {
			$new_tax_obj = get_taxonomy( $new_tax );
			if( isset($new_tax_obj->include_in_filters) && $new_tax_obj->include_in_filters == true ) {
				$addition_attr_tax  = wp_get_post_terms( $car_id, $new_tax_obj->name );
				$addition_attr      = ( ! is_wp_error( $addition_attr_tax ) && empty( $addition_attr_tax ) ) ? '&nbsp;' : $addition_attr_tax[0]->name;
				$product_plain     .= PHP_EOL . esc_html( $new_tax_obj->label ) . ' : ' . $addition_attr;
			}
		}

		return $product_plain;
	}
}

if ( ! function_exists( 'cdhl_get_html_mail_body' ) ) {
	/**
	 * Function For HTML Mail Body
	 *
	 * @param int $car_id car id.
	 */
	function cdhl_get_html_mail_body( $car_id ) {
		$product          = '';
		$product          = '<table class="compare-list compare-datatable" width="100%" border="1" cellspacing="0" cellpadding="0">';
		$product         .= '<tbody>';
		$product         .= '<tr class="image">';
		$product         .= '<td colspan=2 style="text-align:center">';
		$product         .= cardealer_get_cars_image( 'car_thumbnail', $car_id );
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="price">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Price', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$sale_price       = ( get_field( 'sale_price', $car_id ) ) ? get_field( 'sale_price', $car_id ) : '&nbsp;';
		$regular_price    = ( get_field( 'regular_price', $car_id ) ) ? get_field( 'regular_price', $car_id ) : '&nbsp;';
		$product         .= '<span>' . esc_html__( 'Sale Price: ', 'cardealer-helper' ) . $sale_price . '</span><span>&nbsp;&nbsp;' . esc_html__( 'Regular Price : ', 'cardealer-helper' ) . $regular_price . '</span>';
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="year">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Year', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$caryear          = wp_get_post_terms( $car_id, 'car_year' );
		$product         .= ( ! is_wp_error( $caryear ) && empty( $caryear ) ) ? '&nbsp;' : $caryear[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="make">';
		$product         .= '<td >';
		$product         .= esc_html__( 'Vehicle Make', 'cardealer-helper' );
		$product         .= '</td >';
		$product         .= '<td >';
		$carmake          = wp_get_post_terms( $car_id, 'car_make' );
		$product         .= ( ! is_wp_error( $carmake ) && empty( $carmake ) ) ? '&nbsp;' : $carmake[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="model">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Model', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carmodel         = wp_get_post_terms( $car_id, 'car_model' );
		$product         .= ( ! is_wp_error( $carmodel ) && empty( $carmodel ) ) ? '&nbsp;' : $carmodel[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="body_style">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle BodyStyle', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carbodystyle     = wp_get_post_terms( $car_id, 'car_body_style' );
		$product         .= ( ! is_wp_error( $carbodystyle ) && empty( $carbodystyle ) ) ? '&nbsp;' : $carbodystyle[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="mileage">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Mileage', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carmileage       = wp_get_post_terms( $car_id, 'car_mileage' );
		$product         .= ( ! is_wp_error( $carmileage ) && empty( $carmileage ) ) ? '&nbsp;' : $carmileage[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="fuel_economy">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Fuel Economy', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carfueleconomy   = wp_get_post_terms( $car_id, 'car_fuel_economy' );
		$product         .= ( ! is_wp_error( $carfueleconomy ) && empty( $carfueleconomy ) ) ? '&nbsp;' : $carfueleconomy[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="transmission">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Transmission', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$cartransmission  = wp_get_post_terms( $car_id, 'car_transmission' );
		$product         .= ( ! is_wp_error( $cartransmission ) && empty( $cartransmission ) ) ? '&nbsp;' : $cartransmission[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="condition">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Condition', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carcondition     = wp_get_post_terms( $car_id, 'car_condition' );
		$product         .= ( ! is_wp_error( $carcondition ) && empty( $carcondition ) ) ? '&nbsp;' : $carcondition[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="drivetrain">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Drivetrain', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$cardrivetrain    = wp_get_post_terms( $car_id, 'car_drivetrain' );
		$product         .= ( ! is_wp_error( $cardrivetrain ) && empty( $cardrivetrain ) ) ? '&nbsp;' : $cardrivetrain[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="engine">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Engine', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carengine        = wp_get_post_terms( $car_id, 'car_engine' );
		$car_engine       = ( ! is_wp_error( $carengine ) && ! empty( $carengine ) ) ? $carengine[0]->name : '&nbsp;';
		$product         .= esc_html__( 'Vehicle Engine', 'cardealer-helper' ) . $car_engine;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="exterior_color">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Exterior Color', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carexteriorcolor = wp_get_post_terms( $car_id, 'car_exterior_color' );
		$product         .= ( ! is_wp_error( $carexteriorcolor ) && empty( $carexteriorcolor ) ) ? '&nbsp;' : $carexteriorcolor[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="interior_color">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Interior Color', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carinteriorcolor = wp_get_post_terms( $car_id, 'car_interior_color' );
		$product         .= ( ! is_wp_error( $carinteriorcolor ) && empty( $carinteriorcolor ) ) ? '&nbsp;' : $carinteriorcolor[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="stock_number">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Stock Number', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carstocknumber   = wp_get_post_terms( $car_id, 'car_stock_number' );
		$product         .= ( ! is_wp_error( $carstocknumber ) && empty( $carstocknumber ) ) ? '&nbsp;' : $carstocknumber[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';
		$product         .= '<tr class="vin_number">';
		$product         .= '<td>';
		$product         .= esc_html__( 'Vehicle Vin Number', 'cardealer-helper' );
		$product         .= '</td>';
		$product         .= '<td>';
		$carvinnumber     = wp_get_post_terms( $car_id, 'car_vin_number' );
		$product         .= ( ! is_wp_error( $carvinnumber ) && empty( $carvinnumber ) ) ? '&nbsp;' : $carvinnumber[0]->name;
		$product         .= '</td>';
		$product         .= '</tr>';

		$taxonomies_raw = get_object_taxonomies( 'cars' );
		foreach ( $taxonomies_raw as $new_tax ) {
			$new_tax_obj = get_taxonomy( $new_tax );
			if( isset($new_tax_obj->include_in_filters) && $new_tax_obj->include_in_filters == true ) {
				//$tax_arr[] = $new_tax;
				$product         .= '<tr class="' . esc_attr($new_tax_obj->name) . '">';
				$product         .= '<td>';
				$product         .= $new_tax_obj->label;
				$product         .= '</td>';
				$product         .= '<td>';
				$addition_attr    = wp_get_post_terms( $car_id, $new_tax_obj->name );
				$product         .= ( ! is_wp_error( $addition_attr ) && empty( $addition_attr ) ) ? '&nbsp;' : $addition_attr[0]->name;
				$product         .= '</td>';
				$product         .= '</tr>';
			}
		}

		$product         .= '</tbody>';
		$product         .= '</table>';
		/**
		 * Filters the mail body for vehicle details contents for dealer forms.
		 *
		 * @since 1.0
		 * @param string     $product   HTML string of the mail body.
		 * $param int        $car_id    Vehicle ID.
		 * @visible          true
		 */
		return apply_filters( 'cdhl_get_html_mail_body', $product, $car_id );
	}
}

<?php
add_filter( 'cardealer_acf_car_data', 'cdhl_acf_car_data_add_additional_attributes');
function cdhl_acf_car_data_add_additional_attributes( $args ) {
	$taxonomies_raw   = get_object_taxonomies( 'cars' );
	$additional_taxes = array();
	foreach ( $taxonomies_raw as $new_tax ) {
		$new_tax_obj = get_taxonomy( $new_tax );
		if ( isset($new_tax_obj->include_in_filters) && $new_tax_obj->include_in_filters == true ) {
			$additional_taxes[$new_tax] = $new_tax_obj;
		}
	}

	if ( is_array($additional_taxes) && count($additional_taxes) > 0 ) {

		$args['fields'][] = array (
			'key'              => 'field_16221e6c69226f8e162b3706', //Must pass unique value here. Do not use blank space and special character just use int or alphabets. Used "http://onlineuuidgenerator.com/time/5/0/0/" to generate unique key
			'label'            => esc_html__('Additional Attributes','cardealer-helper'),
			'name'             => '',
			'type'             => 'tab',
			'instructions'     => '',
			'required'         => 0,
			'conditional_logic'=> 0,
			'wrapper'          => array (
				'width'=> '',
				'class'=> '',
				'id'   => '',
			),
			'placement' => 'left',
			'endpoint'  => 0,
		);

		foreach ( $additional_taxes as $additional_tax_key => $additional_tax ) {
			$instructions = '';
			if ( isset( $additional_tax->source_additional_attribute ) && true === $additional_tax->source_additional_attribute ) {
				$tax_url = add_query_arg( array(
					'post_type' => 'cars',
					'taxonomy'  => $additional_tax_key,
				), admin_url( 'edit-tags.php' ) );

				$instructions = sprintf(
					wp_kses(
						/* Translators: %1$s Link to attributes panel. */
						__( 'Click <a href="%1$s" target="_blank">here</a> to manage items of this attribute.', 'cardealer-helper' ),
						array(
							'a'      => array(
								'href'   => true,
								'target' => true,
							),
						)
					),
					esc_url( $tax_url )
				);
			}

			$args['fields'][] = array (
				'key' => 'field_' . $additional_tax_key . '5dfb4bdad05644b3b67f9f4d3f1dac50',
				'label' => $additional_tax->label,
				'name' => 'field_additional_tax_' . $additional_tax_key,
				'type' => 'taxonomy',
				'instructions' => $instructions,
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => $additional_tax_key,
				'field_type' => 'select',
				'multiple' => 0,
				'allow_null' => 1,
				'return_format' => 'id',
				'add_term' => 1,
				'load_terms' => 1,
				'save_terms' => 1,
			);
		}
	}

    return $args;
}

<?php
/**
 * CarDealer Redux Freamwork init.
 *
 * @package car-dealer-helper/functions
 */

if ( ! class_exists( 'Redux' ) ) {
	return;
}


if ( ! function_exists( 'cdhl_remove_redux_demo' ) ) {
	/**
	 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
	 */
	function cdhl_remove_redux_demo() {
		// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
		if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
			remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::instance(), 'plugin_metalinks' ), null, 2 );

			// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
			remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
		}
	}
}
add_action( 'redux/loaded', 'cdhl_remove_redux_demo' );

require_once trailingslashit( CDHL_PATH ) . 'includes/redux/redux-options.php';           // Redux Core & Options
require_once trailingslashit( CDHL_PATH ) . 'includes/redux/extensions.php';// Load Redux modified fields

add_action( 'admin_bar_menu', 'cdhl_toolbar_theme_options_link', 999 );

/**
 * Set theme options link in toolbar.
 *
 * @return void
 */
function cdhl_toolbar_theme_options_link( $wp_admin_bar ) {
	$args = array(
		'id'    => 'cardealer',
		'title' => '<span class="cd_toolbar_btn"><img alt="cd_toolbar" src="' . esc_url( CDHL_URL ) . '/images/menu-icon.png"></span><a href="' . esc_url_raw( admin_url( "admin.php?page=cardealer" ) ) . '">Theme Options</a>',
		'meta'  => array(
			'class' => 'wp-admin-bar-cardealer-link',
		),
	);
	$wp_admin_bar->add_node( $args );
}

add_action( 'admin_menu', 'cdhl_remove_redux_menu', 12 );

/**
 * Remove redux menu under the tools.
 */
function cdhl_remove_redux_menu() {
	remove_submenu_page( 'tools.php', 'redux-about' );
}

// Hide advertisement in Redux Options.
add_filter( 'redux/' . 'car_dealer_options' . '/aURL_filter', '__return_true' );


if ( ! function_exists( 'cdhl_redux_search_options' ) ) {

	/**
	 * Redux Search Option.
	 */
	function cdhl_redux_search_options() {
		if ( class_exists( 'Redux' ) ) {
			global $opt_name;

			$redux_sections = Redux::getSections( $opt_name );

			$options_object = 0;
			$import_export  = 0;
			$section_id     = 0;
			$index          = 0;

			$option_fields = array();

			foreach ( $redux_sections as $key => $section ) {
				if ( isset( $section['title'] ) && ( isset( $section['type'] ) && 'section' !== (string) $section['type'] ) || isset( $section['icon'] ) ) {
					$option_fields[ $index ]['title']      = $section['title'];
					$option_fields[ $index ]['id']         = $section['id'];
					$option_fields[ $index ]['path']       = $section['title'];
					$option_fields[ $index ]['section_id'] = $section_id;

					if ( isset( $section['icon'] ) ) {
						$option_fields[ $index ]['icon'] = $section['icon'];
					}
				}
				if ( 'options-object' === (string) $key ) {
					$options_object = 1;
				}
				if ( 'import/export' === (string) $key ) {
					$import_export = 1;
				}

				if ( isset( $section['fields'] ) ) {
					foreach ( $section['fields'] as $field ) {
						if ( isset( $field['title'] ) && ( ( isset( $field['type'] ) && 'section' !== (string) $field['type'] ) ) ) {
							$index++;
							$option_fields[ $index ]['id']         = $field['id'];
							$option_fields[ $index ]['title']      = $field['title'];
							$option_fields[ $index ]['path']       = $section['title'] . ' -> ' . $field['title'];
							$option_fields[ $index ]['section_id'] = $section_id;

							if ( isset( $section['icon'] ) ) {
								$option_fields[ $index ]['icon'] = $section['icon'];
							}
						}
					}
				} else {
					$index++;
				}
				$section_id++;
			}

			if ( 0 === (int) $import_export ) {
				$option_fields[] = array(
					'title'      => esc_html( 'Import / Export', 'cardealer-helper' ),
					'id'         => 'redux_import_export',
					'path'       => esc_html( 'Import / Export', 'cardealer-helper' ),
					'section_id' => 42,
					'icon'       => 'el el-refresh',
				);
			}

			$localize_data['search_option_placeholder_text'] = esc_js( __( 'Search for Theme options', 'cardealer-helper' ) );
			$localize_data['reduxThemeOptions']              = $option_fields;

			return apply_filters( 'cardealer_admin_search_options_localize_data', $localize_data );
		}
	}
}

<?php
/**
 * CarDealer Visual Composer vehicles search Shortcode
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package car-dealer-helper/functions
 */

add_shortcode( 'cd_vehicles_search', 'cdhl_cd_vehicles_search_shortcode' );

/**
 * Shortcode HTML.
 *
 * @param array $atts .
 */
function cdhl_cd_vehicles_search_shortcode( $atts ) {
	extract(
		shortcode_atts(
			array(
				'search_style'          => 'style_1',
				'search_condition_tabs' => '',
				'section_title'         => esc_html__( 'I want to Buy', 'cardealer-helper' ),
				'filter_position'       => 'top',
				'filter_background'     => 'dark',
				'src_button_label'      => esc_html__( 'Search Inventory', 'cardealer-helper' ),
			),
			$atts
		)
	);
	extract( $atts );

	if ( empty( $search_style ) ) {
		return;
	}
	// Vehicle Serach Criterias.
	$carsfilters = array(
		'car_year',
		'car_make',
		'car_model',
	);
	ob_start();
	$uid               = uniqid();
	$tab_label         = apply_filters( 'search_cars_tab_label', $section_title );
	$matching_vehicles = 0;
	if ( 'style_1' === (string) $search_style ) {
		?>
		<div class="slider-content vehicle-search-section text-center clearfix box <?php echo esc_attr( $filter_position ); ?> <?php echo esc_attr( $filter_background ); ?>">
			<div class="row sort-filters-box search-tab">
				<div id="tabs-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabs">
					<h6> <?php echo esc_html( $tab_label ); ?></h6>
					<?php
					if ( $search_condition_tabs ) {
						$search_condition_tabs = explode( ',', $search_condition_tabs ); }
					?>

					<ul class="tabs text-left">
					<?php if ( empty( $search_condition_tabs ) ) { ?>
						<li data-tabs="all-cars-<?php echo esc_attr( $uid ); ?>" class="active">  <?php esc_html_e( 'All vehicles', 'cardealer-helper' ); ?></li>
						<li data-tabs="new-cars-<?php echo esc_attr( $uid ); ?>"> <?php esc_html_e( 'New Vehicles', 'cardealer-helper' ); ?> </li>
						<li data-tabs="used-cars-<?php echo esc_attr( $uid ); ?>"> <?php esc_html_e( 'Used Vehicles', 'cardealer-helper' ); ?> </li>
						<li data-tabs="certified-<?php echo esc_attr( $uid ); ?>"> <?php esc_html_e( 'Certified', 'cardealer-helper' ); ?> </li>
					<?php } else { ?>
						<?php $first_position = $search_condition_tabs[0]; ?>
						<?php if ( in_array( 'all_vehicles', $search_condition_tabs, true ) ) { ?>
							<li data-tabs="all-cars-<?php echo esc_attr( $uid ); ?>" class="<?php echo ( 'all_vehicles' === (string) $first_position ) ? 'active' : ''; ?>">  <?php esc_html_e( 'All vehicles', 'cardealer-helper' ); ?></li>
						<?php } ?>
						<?php if ( in_array( 'new_vehicles', $search_condition_tabs, true ) ) { ?>
							<li data-tabs="new-cars-<?php echo esc_attr( $uid ); ?>" class="<?php echo ( 'new_vehicles' === (string) $first_position ) ? 'active' : ''; ?>"> <?php esc_html_e( 'New Vehicles', 'cardealer-helper' ); ?> </li>
						<?php } ?>
						<?php if ( in_array( 'used_vehicles', $search_condition_tabs, true ) ) { ?>
							<li data-tabs="used-cars-<?php echo esc_attr( $uid ); ?>" class="<?php echo ( 'used_vehicles' === (string) $first_position ) ? 'active' : ''; ?>"> <?php esc_html_e( 'Used Vehicles', 'cardealer-helper' ); ?> </li>
						<?php } ?>
						<?php if ( in_array( 'certified', $search_condition_tabs, true ) ) { ?>
							<li data-tabs="certified-<?php echo esc_attr( $uid ); ?>" class="<?php echo ( 'certified' === (string) $first_position ) ? 'active' : ''; ?>"> <?php esc_html_e( 'Certified', 'cardealer-helper' ); ?> </li>
						<?php } ?>
					<?php } ?>
					</ul>
					<?php if ( empty( $search_condition_tabs ) ) { ?>
					<div id="all-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="all">
						<form>
							<?php
							global $wpdb;
							if ( cardealer_is_wpml_active() ) {
								$all_cars = "SELECT count(p.id) FROM $wpdb->posts p LEFT JOIN " . $wpdb->prefix . "icl_translations t
								ON p.ID = t.element_id
								WHERE p.post_type = 'cars' 
								AND p.post_status = 'publish'
								AND t.language_code = '" . ICL_LANGUAGE_CODE . "'";
							} else {
								$all_cars = "SELECT count(p.id) FROM $wpdb->posts p WHERE p.post_type = 'cars' and p.post_status = 'publish'";
							}

							$matching_vehicles = $wpdb->get_var( $all_cars );
							foreach ( $carsfilters as $filters ) :
								$tax_terms     = cdhl_get_terms(
									array(
										'taxonomy' => $filters,
										'orderby'  => 'name',
									)
								);
								$taxonomy_name = get_taxonomy( $filters );
								$label         = $taxonomy_name->labels->menu_name;
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="all-cars_sort_<?php echo esc_attr( $filters . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $filters ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $filters ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $tax_terms as $key => $term ) :
												?>
												<option value="<?php echo esc_attr( $term ); ?>"><?php echo esc_html( $key ); ?></option>
												<?php
											endforeach;
											?>
										</select>
									</div>
								</div>
								<?php
							endforeach;
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php echo esc_html__( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<div id="new-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="new">
						<form>
							<input type="hidden" name="car_condition" value="new" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'New', 'NEW', 'new', 'N', 'n' ) );
							if ( ! empty( $car_terms ) ) {
								foreach ( $car_terms as $terms ) {
									$label             = $terms['tax_label'];
									$matching_vehicles = $terms['vehicles_matched'];
									?>
									<div class="col-lg-2 col-md-2 col-sm-4">
										<div class="selected-box">
											<select data-uid="<?php echo esc_attr( $uid ); ?>" id="new-cars_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
												<option value=""><?php echo esc_html( $label ); ?></option>
												<?php
												foreach ( $terms['tax_terms'] as $key => $term ) {
													?>
													<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
													<?php
												}
												?>
											</select>
										</div>
									</div>
									<?php
								}
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php echo esc_html__( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<div id="used-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="used">
						<form>
							<input type="hidden" name="car_condition" value="used" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'Used', 'USED', 'used', 'U', 'u' ) );
							foreach ( $car_terms as $terms ) {
								$label             = $terms['tax_label'];
								$matching_vehicles = $terms['vehicles_matched'];
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="used-cars_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $terms['tax_terms'] as $key => $term ) {
												?>
												<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
												<?php
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>

					<div id="certified-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="certified">
						<form>
							<input type="hidden" name="car_condition" value="certified" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'Certified', 'certified', 'CERTIFIED', 'C', 'c' ) );
							foreach ( $car_terms as $terms ) {
								$label             = $terms['tax_label'];
								$matching_vehicles = $terms['vehicles_matched'];
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="certified_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $terms['tax_terms'] as $key => $term ) {
												?>
												<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
												<?php
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
				<?php } else { ?> 
						<?php if ( ! empty( $search_condition_tabs ) && 'all_vehicles' === (string) $search_condition_tabs[0] ) { ?>
					<div id="all-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="all">
						<form>
							<?php
							global $wpdb;
							$all_cars          = "SELECT count(DISTINCT pm.post_id) FROM $wpdb->postmeta pm JOIN $wpdb->posts p ON (p.ID = pm.post_id) WHERE p.post_type = 'cars' and p.post_status = 'publish'";
							$matching_vehicles = $wpdb->get_var( $all_cars );
							foreach ( $carsfilters as $filters ) :
								$tax_terms     = cdhl_get_terms(
									array(
										'taxonomy' => $filters,
										'orderby'  => 'name',
									)
								);
								$taxonomy_name = get_taxonomy( $filters );
								$label         = $taxonomy_name->labels->menu_name;
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="all-cars_sort_<?php echo esc_attr( $filters . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $filters ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $filters ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $tax_terms as $key => $term ) :
												?>
												<option value="<?php echo esc_attr( $term ); ?>"><?php echo esc_html( $key ); ?></option>
												<?php
											endforeach;
											?>
										</select>
									</div>
								</div>
								<?php
							endforeach;
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<?php } ?>
						<?php if ( ! empty( $search_condition_tabs ) && 'new_vehicles' === (string) $search_condition_tabs[0] ) { ?>
					<div id="new-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="new">
						<form>
							<input type="hidden" name="car_condition" value="new" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'New', 'NEW', 'new', 'N', 'n' ) );
							if ( ! empty( $car_terms ) ) {
								foreach ( $car_terms as $terms ) {
									$label             = $terms['tax_label'];
									$matching_vehicles = $terms['vehicles_matched'];
									?>
									<div class="col-lg-2 col-md-2 col-sm-4">
										<div class="selected-box">
											<select data-uid="<?php echo esc_attr( $uid ); ?>" id="new-cars_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
												<option value=""><?php echo esc_html( $label ); ?></option>
												<?php
												foreach ( $terms['tax_terms'] as $key => $term ) {
													?>
													<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
																			<?php
												}
												?>
											</select>
										</div>
									</div>
									<?php
								}
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<?php } ?>
						<?php if ( ! empty( $search_condition_tabs ) && 'used_vehicles' === (string) $search_condition_tabs[0] ) { ?>
					<div id="used-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="used">
						<form>
							<input type="hidden" name="car_condition" value="used" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'Used', 'USED', 'used', 'U', 'u' ) );
							foreach ( $car_terms as $terms ) {
								$label             = $terms['tax_label'];
								$matching_vehicles = $terms['vehicles_matched'];
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="used-cars_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $terms['tax_terms'] as $key => $term ) {
												?>
												<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
																		<?php
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<?php } ?>
						<?php if ( ! empty( $search_condition_tabs ) && 'certified' === (string) $search_condition_tabs[0] ) { ?>
					<div id="certified-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="certified">
						<form>
							<input type="hidden" name="car_condition" value="certified" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'Certified', 'certified', 'CERTIFIED', 'C', 'c' ) );
							foreach ( $car_terms as $terms ) {
								$label             = $terms['tax_label'];
								$matching_vehicles = $terms['vehicles_matched'];
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="certified_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $terms['tax_terms'] as $key => $term ) {
												?>
												<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
																		<?php
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>	
					<?php } ?>
				<?php } ?>
					<?php if ( ! empty( $search_condition_tabs ) && in_array( 'all_vehicles', $search_condition_tabs, true ) && 'all_vehicles' !== (string) $search_condition_tabs[0] ) { ?>
					<div id="all-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="all">
						<form>
							<?php
							global $wpdb;
							$all_cars          = "SELECT count(DISTINCT pm.post_id) FROM $wpdb->postmeta pm JOIN $wpdb->posts p ON (p.ID = pm.post_id) WHERE p.post_type = 'cars' and p.post_status = 'publish'";
							$matching_vehicles = $wpdb->get_var( $all_cars );
							foreach ( $carsfilters as $filters ) :
								$tax_terms     = cdhl_get_terms(
									array(
										'taxonomy' => $filters,
										'orderby'  => 'name',
									)
								);
								$taxonomy_name = get_taxonomy( $filters );
								$label         = $taxonomy_name->labels->menu_name;
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="all-cars_sort_<?php echo esc_attr( $filters . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $filters ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $filters ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $tax_terms as $key => $term ) :
												?>
												<option value="<?php echo esc_attr( $term ); ?>"><?php echo esc_html( $key ); ?></option>
												<?php
											endforeach;
											?>
										</select>
									</div>
								</div>
								<?php
							endforeach;
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<?php } ?>
					<?php if ( ! empty( $search_condition_tabs ) && in_array( 'new_vehicles', $search_condition_tabs, true ) && 'new_vehicles' !== (string) $search_condition_tabs[0] ) { ?>
					<div id="new-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="new">
						<form>
							<input type="hidden" name="car_condition" value="new" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'New', 'NEW', 'new', 'N', 'n' ) );
							if ( ! empty( $car_terms ) ) {
								foreach ( $car_terms as $terms ) {
									$label             = $terms['tax_label'];
									$matching_vehicles = $terms['vehicles_matched'];
									?>
									<div class="col-lg-2 col-md-2 col-sm-4">
										<div class="selected-box">
											<select data-uid="<?php echo esc_attr( $uid ); ?>" id="new-cars_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
												<option value=""><?php echo esc_html( $label ); ?></option>
												<?php
												foreach ( $terms['tax_terms'] as $key => $term ) {
													?>
													<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
																			<?php
												}
												?>
											</select>
										</div>
									</div>
									<?php
								}
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<?php } ?>
					<?php if ( ! empty( $search_condition_tabs ) && in_array( 'used_vehicles', $search_condition_tabs, true ) && 'used_vehicles' !== (string) $search_condition_tabs[0] ) { ?>
					<div id="used-cars-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="used">
						<form>
							<input type="hidden" name="car_condition" value="used" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'Used', 'USED', 'used', 'U', 'u' ) );
							foreach ( $car_terms as $terms ) {
								$label             = $terms['tax_label'];
								$matching_vehicles = $terms['vehicles_matched'];
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="used-cars_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $terms['tax_terms'] as $key => $term ) {
												?>
												<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
																		<?php
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
					<?php } ?>
					<?php if ( ! empty( $search_condition_tabs ) && in_array( 'certified', $search_condition_tabs, true ) && 'certified' !== (string) $search_condition_tabs[0] ) { ?>
					<div id="certified-<?php echo esc_attr( $uid ); ?>" class="cardealer-tabcontent" data-condition="certified">
						<form>
							<input type="hidden" name="car_condition" value="certified" />
							<?php
							$car_terms = cdhl_get_car_attrs_by_condition( array( 'Certified', 'certified', 'CERTIFIED', 'C', 'c' ) );
							foreach ( $car_terms as $terms ) {
								$label             = $terms['tax_label'];
								$matching_vehicles = $terms['vehicles_matched'];
								?>
								<div class="col-lg-2 col-md-2 col-sm-4">
									<div class="selected-box">
										<select data-uid="<?php echo esc_attr( $uid ); ?>" id="certified_sort_<?php echo esc_attr( $terms['taxonomy'] . '_' . $uid ); ?>" data-id="<?php echo esc_attr( $terms['taxonomy'] ); ?>" class="selectpicker search-filters-box col-4 cd-select-box" name="<?php echo esc_attr( $terms['taxonomy'] ); ?>">
											<option value=""><?php echo esc_html( $label ); ?></option>
											<?php
											foreach ( $terms['tax_terms'] as $key => $term ) {
												?>
												<option value="<?php echo esc_attr( $term['slug'] ); ?>"><?php echo esc_html( $term['name'] ); ?></option>								
												<?php
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							?>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input id="vehicle_location" type="text" placeholder="<?php esc_html_e( 'Location', 'cardealer-helper' ); ?>" class="form-control placeholder" name="vehicle_location">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="text-center">
									<button class="button btn-block red csb-submit-btn" type="button">
									<?php
										echo ( ! empty( $src_button_label ) ) ? esc_html( $src_button_label ) : esc_html__( 'Search', 'cardealer-helper' )// phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE
									?>
									</button>
								</div>
							</div>
							<div class="car-total pull-right">
								<h5 class="text-white"><i class="fas fa-caret-right"></i>(<p class="matching-vehicles"><?php echo esc_html( $matching_vehicles ); ?></p>) <span class="text-red"><?php esc_html_e( 'Vehicles', 'cardealer-helper' ); ?></span></h5>
							</div>
						</form>
					</div>
				<?php } ?>

				</div>
			</div>
		<div class="clearfix"></div>
		<div class="filter-loader"></div>
		</div>
		<?php
	}
	return ob_get_clean();
}

/**
 * Shortcode mapping.
 *
 * @return void
 */
function cdhl_vehicle_search_shortcode_integrateWithVC() {
	if ( function_exists( 'vc_map' ) ) {
		$cars_taxonomy = cdhl_get_cars_taxonomy();
		vc_map(
			array(
				'name'     => esc_html__( 'Potenza Vehicles Search', 'cardealer-helper' ),
				'base'     => 'cd_vehicles_search',
				'class'    => '',
				'category' => esc_html__( 'Potenza', 'cardealer-helper' ),
				'icon'     => cardealer_vc_shortcode_icon( 'cd_vehicles_search' ),
				'params'   => array(
					array(
						'type'       => 'cd_radio_image_2',
						'heading'    => esc_html__( 'Search Style', 'cardealer-helper' ),
						'param_name' => 'search_style',
						'options'    => array(
							array(
								'value' => 'style_1',
								'title' => esc_html__( 'Style 1', 'cardealer-helper' ),
								'image' => trailingslashit( CDHL_VC_URL ) . 'vc_images/options/cd_vehicle_search/style-1.png',
							),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Section Title', 'cardealer-helper' ),
						'description' => esc_html__( 'Enter search section title', 'cardealer-helper' ),
						'param_name'  => 'section_title',
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Search Vehicle Conditions Tab', 'cardealer-helper' ),
						'param_name'  => 'search_condition_tabs',
						'description' => esc_html__( 'Select search vehicle conditions in tabs. If no selected, then all display.', 'cardealer-helper' ),
						'value'       => array(
							esc_html__( 'All vehicles', 'cardealer-helper' ) => 'all_vehicles',
							esc_html__( 'New vehicles', 'cardealer-helper' ) => 'new_vehicles',
							esc_html__( 'Used vehicles', 'cardealer-helper' ) => 'used_vehicles',
							esc_html__( 'Certified', 'cardealer-helper' )    => 'certified',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Filters Position', 'cardealer-helper' ),
						'param_name'  => 'filter_position',
						'value'       => array(
							esc_html__( 'Top', 'cardealer-helper' ) => 'top',
							esc_html__( 'Default', 'cardealer-helper' ) => 'default',
						),
						'save_always' => true,
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Filters Background', 'cardealer-helper' ),
						'param_name'  => 'filter_background',
						'value'       => array(
							esc_html__( 'Dark', 'cardealer-helper' )   => 'dark',
							esc_html__( 'Light', 'cardealer-helper' )  => 'light',
						),
						'save_always' => true,
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Button label', 'cardealer-helper' ),
						'description' => esc_html__( 'Enter search button label', 'cardealer-helper' ),
						'param_name'  => 'src_button_label',
						'value'       => esc_html__( 'Search Inventory', 'cardealer-helper' ),
					),
				),
			)
		);
	}
}
add_action( 'vc_before_init', 'cdhl_vehicle_search_shortcode_integrateWithVC' );

<?php
/**
 * Theme vehicle compare functions.
 *
 * @author  TeamWP @Potenza Global Solutions
 * @package cardealer-helper-library/functions
 * @version 1.0.0
 */

if ( ! function_exists( 'cdhl_compare_popup_title' ) ) {
	/**
	 * Compare popup title
	 *
	 * @param bool $echo .
	 */
	function cdhl_compare_popup_title( $echo = true ) {
		$popup_title = esc_html__( 'Compare Vehicles', 'cardealer-helper' );
		/**
		 * Filters the title display on the compare vehicle pop-up.
		 *
		 * @since 1.0
		 * @param string    $popup_title    Title of the compare vehicle pop-up.
		 * @visible         true
		 */
		$popup_title = apply_filters( 'cdhl_compare_popup_title', $popup_title );
		if ( $echo ) {
			echo esc_html( $popup_title );
		} else {
			return $popup_title;
		}
	}
}

if ( ! function_exists( 'cdhl_compare_column_fields' ) ) {
	/**
	 * Compare column field
	 */
	function cdhl_compare_column_fields() {

		$data = array(
			'remove'           => '',
			'car_image'        => '',
			'price'            => esc_html__( 'Price', 'cardealer-helper' ),
			'year'             => esc_html__( 'Year', 'cardealer-helper' ),
			'make'             => esc_html__( 'Make', 'cardealer-helper' ),
			'model'            => esc_html__( 'Model', 'cardealer-helper' ),
			'body_style'       => esc_html__( 'Body Style', 'cardealer-helper' ),
			'mileage'          => esc_html__( 'Millaje'),	
			'fuel_economy'     => esc_html__( 'Fuel Economy', 'cardealer-helper' ),
			'transmission'     => esc_html__( 'Transmission', 'cardealer-helper' ),
			'condition'        => esc_html__( 'Condition', 'cardealer-helper' ),
			'drivetrain'       => esc_html__( 'Drivetrain', 'cardealer-helper' ),
			'engine'           => esc_html__( 'Engine', 'cardealer-helper' ),
			'exterior_color'   => esc_html__( 'Exterior Color', 'cardealer-helper' ),
			'interior_color'   => esc_html__( 'Interior Color', 'cardealer-helper' ),
			'stock_number'     => esc_html__( 'Stock Number', 'cardealer-helper' ),
			'vin_number'       => esc_html__( 'Vin Number', 'cardealer-helper' ),
			'features_options' => esc_html__( 'Features & Options', 'cardealer-helper' ),
		);

		$taxonomies_raw = get_object_taxonomies( 'cars' );
		$tax_arr = array();
		foreach ( $taxonomies_raw as $new_tax ) {
			$new_tax_obj = get_taxonomy( $new_tax );
			if( isset($new_tax_obj->include_in_filters) && $new_tax_obj->include_in_filters == true ) {
				$tax_arr[$new_tax_obj->name] = $new_tax_obj->label;
			}
		}

		$result_array = $data;
		if ( ! empty( $tax_arr ) ) {
			$result_array = array_slice($data, 0, 17, true) +
			$tax_arr +
			array_slice($data, 17, count($data) - 1, true) ;
		}

		/**
		 * Filters the array of vehicle attributes label displayed in compare vehicle pop-up.
		 *
		 * @since 1.0
		 * @return          Array of vehicle attributes label displayed in compare pop-up.
		 * @visible         true
		 */
		$data = apply_filters( 'cdhl_compare_column_fields', $result_array );
		return $data;
	}
}


/**
 * Compare column delete, image, price
 *
 * @see cdhl_compare_column_delete()
 * @see cdhl_compare_column_image()
 * @see cdhl_compare_column_price()
 */
add_action( 'cdhl_compare_column_before_attributes', 'cdhl_compare_column_delete', 10, 2 );
add_action( 'cdhl_compare_column_before_attributes', 'cdhl_compare_column_image', 20, 2 );
add_action( 'cdhl_compare_column_before_attributes', 'cdhl_compare_column_price', 30, 2 );
if ( ! function_exists( 'cdhl_compare_column_delete' ) ) {
	/**
	 * Compare column delete
	 *
	 * @param string $class .
	 * @param string $car_id .
	 */
	function cdhl_compare_column_delete( $class, $car_id ) {
		?>
		<tr class="delete">
			<td class="<?php echo esc_attr( $class ); ?>" data-id="<?php echo esc_attr( $car_id ); ?>">
				<a href="javascript:void(0)" data-car_id="<?php echo esc_attr( $car_id ); ?>" class="drop_item"><span class="remove">x</span></a>
			</td>
		</tr>
		<?php
	}
}

if ( ! function_exists( 'cdhl_compare_column_image' ) ) {
	/**
	 * Compare column image
	 *
	 * @param string $class .
	 * @param string $car_id .
	 */
	function cdhl_compare_column_image( $class, $car_id ) {
		$carlink = get_permalink( $car_id );
		?>
		<tr class="image">
			<td class="<?php echo esc_attr( $class ); ?>" data-id="<?php echo esc_attr( $car_id ); ?>">
				<a href="<?php echo esc_url( $carlink ); ?>">
					<?php
					if ( function_exists( 'cardealer_get_cars_image' ) ) {
						echo wp_kses( cardealer_get_cars_image( 'car_thumbnail', $car_id ), cardealer_allowed_html( array( 'img' ) ) );
					}
					?>
				</a>
			</td>
		</tr>
		<?php
	}
}

if ( ! function_exists( 'cdhl_compare_column_price' ) ) {
	/**
	 * Compare column price
	 *
	 * @param string $class .
	 * @param string $car_id .
	 */
	function cdhl_compare_column_price( $class, $car_id ) {
		?>
		<tr class="price car-item">
			<td class="<?php echo esc_attr( $class ); ?>" data-id="<?php echo esc_attr( $car_id ); ?>">
				<?php cardealer_car_price_html( '', $car_id, false, true ); ?>
			</td>
		</tr>
		<?php
	}
}

/**
 * Column delete
 *
 * @see cdhl_compare_column_attributes_data()
 */
add_action( 'cdhl_compare_column_attributes', 'cdhl_compare_column_attributes_data', 10, 3 );
if ( ! function_exists( 'cdhl_compare_column_attributes_data' ) ) {
	/**
	 * Compare column delete
	 *
	 * @param string $compare_fields .
	 * @param string $class .
	 * @param string $car_id .
	 */
	function cdhl_compare_column_attributes_data( $compare_fields, $class, $car_id ) {

		$taxonomies_raw = get_object_taxonomies( 'cars' );
		$additional_taxonomies = array();
		foreach ( $taxonomies_raw as $new_tax ) {
			if ( in_array( $new_tax, $taxonomies ) ) {
				continue;
			}

			$new_tax_obj = get_taxonomy( $new_tax );
			if( isset($new_tax_obj->include_in_filters) && $new_tax_obj->include_in_filters == true ) {
				$additional_taxonomies[] = $new_tax;
			}
		}

		$cars_taxonomy_array = cdhl_get_cars_taxonomy();
		foreach ( $compare_fields as $key => $val ) {
			$cars_taxonomy = "car_$key";
			if ( in_array( $cars_taxonomy, $cars_taxonomy_array, true ) ) {
				?>
				<tr class="<?php echo esc_attr( $key ); ?>">
					<td class="<?php echo esc_attr( $class ); ?>" data-id="<?php echo esc_attr( $car_id ); ?>">
						<?php
						$car_year = wp_get_post_terms( $car_id, $cars_taxonomy );
						if ( ! isset( $car_year ) || empty( $car_year ) ) {
							echo '&nbsp;';
						} else {
							echo esc_html( $car_year[0]->name );
						}
						?>
					</td>
				</tr>
				<?php
			} elseif( ! empty($additional_taxonomies) && in_array( $key, $additional_taxonomies, true ) ) {
				?>
				<tr class="<?php echo esc_attr( $key ); ?>">
					<td class="<?php echo esc_attr( $class ); ?>" data-id="<?php echo esc_attr( $car_id ); ?>">
						<?php
						$addi_attr = wp_get_post_terms( $car_id, $key );
						if ( ! isset( $addi_attr ) || empty( $addi_attr ) ) {
							echo '&nbsp;';
						} else {
							echo esc_html( $addi_attr[0]->name );
						}
						?>
					</td>
				</tr>
				<?php
			}

			if ( 'features_options' === (string) $key ) {
				?>
				<tr class="<?php echo esc_attr( $key ); ?>">
					<td class="<?php echo esc_attr( $class ); ?>" data-id="<?php echo esc_attr( $car_id ); ?>">
						<div>
							<?php
							$car_features_options = wp_get_post_terms( $car_id, 'car_features_options' );
							$json                 = wp_json_encode( $car_features_options ); // Conver Obj to Array.
							$car_features_options = json_decode( $json, true ); // Conver Obj to Array.
							$name_array           = array_map(
								function ( $options ) {
									return $options['name'];
								},
								(array) $car_features_options
							); // get all name term array.
							$options              = implode( ',', $name_array );
							$options_data         = ( ! isset( $options ) || empty( $options ) ) ? '&nbsp;' : $options;
							$html                 = $options_data;
							echo esc_html( $html );
							?>
						</div>
					</td>
				</tr>
				<?php
			}
		}
	}
}


<?php
/**
 * User add new car
 * This template can be overridden by copying it to yourtheme/cardealer-front-submission/cars/cars-add.php.
 *
 * @author  PotenzaGlobalSolutions
 * @package CDFS
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'cdfs_add_car_page_start' );

$car_edit = false;

if ( ! empty( $_GET['edit-car'] ) && wp_unslash( $_GET['edit-car'] ) ) {
	$car_edit = true;
}

$restricted = false;

if ( is_user_logged_in() ) {
	$user    = wp_get_current_user();
	$user_id = $user->ID;
}

$vars = array();

if ( $car_edit ) {
	if ( ! is_user_logged_in() ) {
		echo '<h4>' . esc_html__( 'Please login.', 'cdfs-addon' ) . '</h4>';
		return false;
	}
	if ( ! isset( $_GET['cdfs_nonce'] ) || ! wp_verify_nonce( wp_unslash( $_GET['cdfs_nonce'] ), 'cdhl-action' ) ) {
		echo '<h4>' . esc_html__( 'Invalid action, try again later.', 'cdfs-addon' ) . '</h4>';
		return false;
	}

	if ( ! empty( $_GET['car-id'] ) ) {
		$car_id = intval( $_GET['car-id'] );

		$car_user = get_post_meta( $car_id, 'cdfs_car_user', true );

		if ( intval( $user_id ) !== intval( $car_user ) ) {
			echo '<h4>' . esc_html__( 'You are not the owner of this vehicle.', 'cdfs-addon' ) . '</h4>';
			return false;
		}
	} else {
		echo '<h4>' . esc_html__( 'No vehicle to edit.', 'cdfs-addon' ) . '</h4>';
		return false;
	}

	$vars = array(
		'id' => $car_id,
	);
} ?>
<div class="cdfs_add_car_page">
	<div class="cdfs_add_car_form cdfs_add_car_form_<?php echo $car_edit; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotE ?>">

		<?php do_action( 'cdfs_before_add_car_form' ); ?>

		<form method="POST" action="" enctype="multipart/form-data" id="cdfs_car_form">
			<?php
			if ( $car_edit ) {
				?>
					<input name="cdfs_action_car_id" value="<?php echo esc_attr( $car_id ); ?>" type="hidden">
				<?php
			} else {
				$action = 'cdfs_add_car'; // phpcs:ignore WordPress.WP.GlobalVariablesOverride
			}
			?>
			<input name="cdfs_car_form_action" class="form-control" value="cdfs_add_car" type="hidden">
			<?php
			if ( isset( $user_id ) ) {
				wp_nonce_field( 'cdfs-car-form', 'cdfs-car-form-nonce-field' );
			}
			?>
			<?php
			global $car_dealer_options;
				cdfs_get_template( 'cars/cars-templates/car-attributes.php', $vars );
				cdfs_get_template( 'cars/cars-templates/cars-image-gallery.php', $vars );
				cdfs_get_template( 'cars/cars-templates/cars-pdf-brochure.php', $vars );

			if ( isset( $car_dealer_options['google_maps_api'] ) && ! empty( $car_dealer_options['google_maps_api'] ) ) {
				cdfs_get_template( 'cars/cars-templates/cars-location.php', $vars );
			} else {
				cdfs_get_template( 'cars/cars-templates/cars-location-admin-notice.php', $vars );
			}

				cdfs_get_template( 'cars/cars-templates/car-additional-info.php', $vars );
				cdfs_get_template( 'cars/cars-templates/cars-excerpt.php', $vars );
			?>
			<?php cdfs_get_template( 'my-user-account/user-details-car-page.php' ); // User details on ajax login. ?>

			<?php if ( cdfs_check_captcha_exists() ) { ?>
			<p class="cdfs-form-row">
				<div class="form-group">
					<div id="car_form_captcha" class="g-recaptcha" data-sitekey="<?php echo esc_attr( cdfs_get_goole_api_keys( 'site_key' ) ); ?>"
						<?php
						if ( ! isset( $user_id ) ) {
							?>
							style="display:none"<?php } ?>></div>
				</div>
			</p>
			<?php } ?>
		</form>
	</div>

	<?php do_action( 'cdfs_after_add_car_form' ); ?>
	<?php
	if ( ! isset( $user_id ) ) {
		$class    = 'disabled';
		$disabled = 'disabled=disabled';
	} else {
		$class    = '';
		$disabled = '';
	}
	$label = ( $car_edit ) ? esc_html__( 'Update Details', 'cdfs-addon' ) : esc_html__( 'Submit Details', 'cdfs-addon' );
	?>
	<p class="cdfs-form-row">
		<div class="form-group">
			<button id="cdfs-submit-car" class="button btn cdfs-submit-car <?php echo esc_html( $class ); ?>" <?php echo esc_html( $disabled ); ?>><?php echo esc_html( $label ); ?></button>
		</div>
	</p>

</div>
<?php
do_action( 'cdfs_add_car_page_end' );

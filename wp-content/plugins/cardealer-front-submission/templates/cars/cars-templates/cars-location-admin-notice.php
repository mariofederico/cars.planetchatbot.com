<?php
if ( current_user_can( 'administrator' ) ) {
	?>
	<div class="cdfs_add_car_form">
		<div class="modal-header">
			<h4 class="modal-title" id="cdfs-car-attributes"><?php esc_html_e( 'Vehicle Location', 'cdfs-addon' ); ?></h4>
		</div>
		<div class="row col-sm-12">
			<p><?php
			printf(
				wp_kses(
					__( 'The Google Maps API key is not provided. Please make sure that Google Maps API keys have been added to the <strong style="color:#313131;"><a href="%1$s">Theme Options > Google API Settings > "Google Maps API Key" field</a></strong>. Also, ensure that the API Account and API Keys are configured and working properly.', 'cdfs-addon' ),
					array(
						'strong' => array(
							'style' => true,
						),
						'a'      => array(
							'href' => true,
						),
					)
				),
				esc_url( admin_url( 'admin.php?page=cardealer' ) )
			);
			?></p>
			<p><?php echo wp_kses(
				__( '<b>Note</b>: This information will be visible only to administrators. No visitors will see this notice.', 'cdfs-addon' ),
				array(
					'b' => array(),
				)
			);
			?></p>
		</div>
	</div>
	<?php
}

DROP TABLE IF EXISTS `wp_icl_translation_batches`;
CREATE TABLE `wp_icl_translation_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tp_id` int(11) DEFAULT NULL,
  `ts_url` text COLLATE utf8mb4_unicode_520_ci,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_icl_translation_batches` WRITE;
INSERT INTO `wp_icl_translation_batches` VALUES ('1','Manual Translations from August the 26th, 2021','','','2021-08-26 17:52:12'), ('2','Manual Translations from August the 27th, 2021','','','2021-08-27 15:59:45'), ('3','Manual Translations from August the 29th, 2021','','','2021-08-29 16:20:41'), ('4','Manual Translations from August the 31st, 2021','','','2021-08-31 14:19:47');
UNLOCK TABLES;

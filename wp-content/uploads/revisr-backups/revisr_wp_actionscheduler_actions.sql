DROP TABLE IF EXISTS `wp_actionscheduler_actions`;
CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  KEY `hook` (`hook`),
  KEY `status` (`status`),
  KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  KEY `args` (`args`),
  KEY `group_id` (`group_id`),
  KEY `last_attempt_gmt` (`last_attempt_gmt`),
  KEY `claim_id` (`claim_id`),
  KEY `claim_id_status_scheduled_date_gmt` (`claim_id`,`status`,`scheduled_date_gmt`)
) ENGINE=MyISAM AUTO_INCREMENT=7528 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_actionscheduler_actions` WRITE;
INSERT INTO `wp_actionscheduler_actions` VALUES ('7518','action_scheduler/migration_hook','complete','2021-08-26 16:14:46','2021-08-26 16:14:46','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1629994486;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1629994486;}','1','1','2021-08-26 16:15:16','2021-08-26 16:15:16','0',''), ('7519','woocommerce_run_update_callback','complete','2021-09-10 16:44:11','2021-09-10 16:44:11','{\"update_callback\":\"wc_update_560_create_refund_returns_page\"}','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631292251;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631292251;}','5','1','2021-09-10 16:44:45','2021-09-10 16:44:45','0',''), ('7520','woocommerce_run_update_callback','complete','2021-09-10 16:44:12','2021-09-10 16:44:12','{\"update_callback\":\"wc_update_560_db_version\"}','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631292252;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631292252;}','5','1','2021-09-10 16:44:45','2021-09-10 16:44:45','0',''), ('7521','action_scheduler/migration_hook','complete','2021-09-10 17:11:06','2021-09-10 17:11:06','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631293866;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631293866;}','1','1','2021-09-10 17:11:27','2021-09-10 17:11:27','0',''), ('7522','action_scheduler/migration_hook','complete','2021-09-10 17:38:20','2021-09-10 17:38:20','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631295500;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631295500;}','1','1','2021-09-10 17:38:53','2021-09-10 17:38:53','0',''), ('7523','action_scheduler/migration_hook','complete','2021-09-10 17:41:54','2021-09-10 17:41:54','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631295714;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631295714;}','1','1','2021-09-10 17:42:58','2021-09-10 17:42:58','0',''), ('7524','action_scheduler/migration_hook','complete','2021-09-10 17:44:02','2021-09-10 17:44:02','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631295842;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631295842;}','1','1','2021-09-10 17:44:30','2021-09-10 17:44:30','0',''), ('7525','action_scheduler/migration_hook','complete','2021-09-10 17:46:46','2021-09-10 17:46:46','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631296006;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631296006;}','1','1','2021-09-10 17:46:54','2021-09-10 17:46:54','0',''), ('7526','action_scheduler/migration_hook','complete','2021-09-10 17:51:22','2021-09-10 17:51:22','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631296282;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631296282;}','1','1','2021-09-10 17:51:41','2021-09-10 17:51:41','0',''), ('7527','action_scheduler/migration_hook','complete','2021-09-10 17:52:41','2021-09-10 17:52:41','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1631296361;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1631296361;}','1','1','2021-09-10 17:52:47','2021-09-10 17:52:47','0','');
UNLOCK TABLES;

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_users` WRITE;
INSERT INTO `wp_users` VALUES ('1','raulblanco','$P$Bg44XtKMDZXN4jBsh4TdVaaXmlSAb31','admin','ralmeron@yahoo.com','http://cars.planetchatbot.com','2021-03-04 12:51:44','','0','admin'), ('2','raul','$P$BxZhbez/S7v.vr8ihb7RLkxjj9Y2GT1','raul','raul.blanco@capital-minds.com','','2021-03-07 22:25:04','','0','Raul Blanco'), ('7','juan','$P$BnW1VjMWDpASp/T4T9zrD46no44llz.','juan','juan.gamarra@capital-minds.com','','2021-04-05 16:57:51','','0','Juan Gamarra'), ('5','cardealer2','$P$Bb5FKmv5ZS6bmeXmw1VzIGj4LXX7Me.','cardealer2','ralmeron@yahoo.com','','2021-03-08 15:12:20','1615216340:$P$BdWsmauBSDuTiWN8KHV4bMGOZ6zzNC1','0','cardealer2 cardealer2'), ('4','supervisor1','$P$BfldePtiJIPqkWgbqYcsmPGy6pqBkb/','supervisor1','rjblanco.almeron@yahoo.com','','2021-03-07 22:53:08','','0','Supervisor 1'), ('6','cardealer1','$P$BNxkn.ffKixNSRix5O/GUFygaBAJaT1','cardealer1','ralmeron@hotmail.com','','2021-03-08 16:22:27','','0','Cardealer1 Cardealer1'), ('8','joao','$P$B73CdRL5a7b6BZyhVwRNY2LXMfj8O./','joao','joaosays@gmail.com','','2021-04-12 13:44:05','1618235046:$P$BDCbpwP8p72wMIl5AnI.L3HATIcqsD1','0','joao'), ('9','7uan','$P$BUVlKmZEU1jstUBuFqy0pud96Bm32t/','7uan','7uanc4r10s@gmail.com','','2021-04-12 20:44:32','1623071519:$P$B3DBDXr5PTZVy6zC5mGjRh8CU0uTtn1','0','7uan'), ('10','i7uanc4r10s','$P$BDA8mcBMC//.Lzj/f72YOTobMTayld.','i7uanc4r10s','i7uanc4r10s@gmail.com','','2021-05-05 14:04:36','','0','i7uanc4r10s'), ('11','lcarrera','$P$Bc3sa2/LY5Az6u2JcBLQJKoj4Xq5iV1','lcarrera','lourdcarrera@gmail.com','','2021-05-05 16:05:46','','0','lcarrera'), ('12','lcarrera2','$P$BF2ehP/BQwjoFkfINsjU0PCyKRZWFA1','lcarrera2','Edilu.edicionlujo@gmail.com','','2021-05-05 16:10:26','1620231026:$P$BAtA0A.b.aCZiwmdiGkBX25oPtQrAV.','0','Lourdes Carrera'), ('13','amena','$P$BkQg8FLwtgIAP/lmqRC0fHI48/wwGX.','amena','amena@intelutions.net','','2021-05-13 20:37:39','1620938259:$P$BYisPb6i7KiiSB8m5ysLNfrVfC536L.','0','amena'), ('14','amena2','$P$BRPBN1q3F5ThhgPm2FCjc5rhppjDLO0','amena2','amena@turnospr.com','','2021-05-13 20:38:50','1620938330:$P$Bk6AsNy0JkMSmhZZC/a0UKfqCZ5.ja0','0','amena2'), ('15','mario772','$P$BnoHUFMBGSre4TzBZYLvz98RQcyUJt/','mario772','mariowork772@gmail.com','','2021-05-15 03:28:18','','0','mario772'), ('16','angel1','$P$BPm0DFSay0kTc8M5jDodWA140jxTcM/','angel1','amena.edilu1@gmail.com','','2021-06-05 20:54:18','1622926459:$P$BcxkzNXrrKXhlvoSWxz50wqbCX2ZHh.','0','Angel Mena'), ('17','angel2','$P$BEtiModLN8RpEdgU8pFzhnbVETDyGh.','angel2','amena.edilu2@gmail.com','','2021-06-05 20:55:16','1622926517:$P$BGXCTPB9MzN9.04nYD3W7g4CdisQ0W1','0','Angel M'), ('18','prueba','$P$Ba3xZGc46pIsFKm/py70GWlhnV9hal/','prueba','ferhoriana@gmail.com','','2021-06-06 20:19:41','1623010950:$P$BSOVpiHI7ltjd/QHneQVT9oefSMHVx1','0','juan gamarra');
UNLOCK TABLES;

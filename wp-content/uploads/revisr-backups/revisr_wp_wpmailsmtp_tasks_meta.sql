DROP TABLE IF EXISTS `wp_wpmailsmtp_tasks_meta`;
CREATE TABLE `wp_wpmailsmtp_tasks_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_wpmailsmtp_tasks_meta` WRITE;
INSERT INTO `wp_wpmailsmtp_tasks_meta` VALUES ('1','wp_mail_smtp_admin_notifications_update','W10=','2021-06-07 00:16:25'), ('2','wp_mail_smtp_admin_notifications_update','W10=','2021-06-08 14:55:46'), ('3','wp_mail_smtp_admin_notifications_update','W10=','2021-06-09 15:19:18'), ('4','wp_mail_smtp_admin_notifications_update','W10=','2021-06-10 17:30:49'), ('5','wp_mail_smtp_admin_notifications_update','W10=','2021-06-15 02:32:47'), ('6','wp_mail_smtp_admin_notifications_update','W10=','2021-06-16 12:28:06'), ('7','wp_mail_smtp_admin_notifications_update','W10=','2021-06-17 15:30:01'), ('8','wp_mail_smtp_admin_notifications_update','W10=','2021-06-18 16:02:10'), ('9','wp_mail_smtp_admin_notifications_update','W10=','2021-06-24 17:54:27'), ('10','wp_mail_smtp_admin_notifications_update','W10=','2021-06-29 18:15:56'), ('11','wp_mail_smtp_admin_notifications_update','W10=','2021-06-30 18:49:39'), ('12','wp_mail_smtp_admin_notifications_update','W10=','2021-07-01 22:33:53'), ('13','wp_mail_smtp_admin_notifications_update','W10=','2021-07-01 22:34:55'), ('14','wp_mail_smtp_admin_notifications_update','W10=','2021-07-03 01:24:10'), ('15','wp_mail_smtp_admin_notifications_update','W10=','2021-07-05 16:24:29'), ('16','wp_mail_smtp_admin_notifications_update','W10=','2021-07-07 01:03:28'), ('17','wp_mail_smtp_admin_notifications_update','W10=','2021-07-07 01:04:32');
UNLOCK TABLES;

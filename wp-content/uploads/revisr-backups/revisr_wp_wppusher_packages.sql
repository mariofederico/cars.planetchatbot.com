DROP TABLE IF EXISTS `wp_wppusher_packages`;
CREATE TABLE `wp_wppusher_packages` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `package` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `repository` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `branch` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'master',
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `ptd` int(11) NOT NULL,
  `host` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `private` int(11) NOT NULL,
  `subdirectory` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_wppusher_packages` WRITE;
INSERT INTO `wp_wppusher_packages` VALUES ('1','','mariofederico/edilucar','master','1','1','1','bb','0','');
UNLOCK TABLES;

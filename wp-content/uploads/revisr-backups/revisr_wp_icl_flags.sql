DROP TABLE IF EXISTS `wp_icl_flags`;
CREATE TABLE `wp_icl_flags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `flag` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `from_template` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_code` (`lang_code`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_icl_flags` WRITE;
INSERT INTO `wp_icl_flags` VALUES ('1','ar','ar.png','0'), ('2','az','az.png','0'), ('3','bg','bg.png','0'), ('4','bn','bn.png','0'), ('5','bs','bs.png','0'), ('6','ca','ca.png','0'), ('7','cs','cs.png','0'), ('8','cy','cy.png','0'), ('9','da','da.png','0'), ('10','de','de.png','0'), ('11','el','el.png','0'), ('12','en','en.png','0'), ('13','eo','eo.png','0'), ('14','es','es.png','0'), ('15','et','et.png','0'), ('16','eu','eu.png','0'), ('17','fa','fa.png','0'), ('18','fi','fi.png','0'), ('19','fr','fr.png','0'), ('20','ga','ga.png','0'), ('21','gl','gl.png','0'), ('22','he','he.png','0'), ('23','hi','hi.png','0'), ('24','hr','hr.png','0'), ('25','hu','hu.png','0'), ('26','hy','hy.png','0'), ('27','id','id.png','0'), ('28','is','is.png','0'), ('29','it','it.png','0'), ('30','ja','ja.png','0'), ('31','ko','ko.png','0'), ('32','ku','ku.png','0'), ('33','lt','lt.png','0'), ('34','lv','lv.png','0'), ('35','mk','mk.png','0'), ('36','mn','mn.png','0'), ('37','ms','ms.png','0'), ('38','mt','mt.png','0'), ('39','ne','ne.png','0'), ('40','nl','nl.png','0'), ('41','no','no.png','0'), ('42','pa','pa.png','0'), ('43','pl','pl.png','0'), ('44','pt-br','pt-br.png','0'), ('45','pt-pt','pt-pt.png','0'), ('46','qu','qu.png','0'), ('47','ro','ro.png','0'), ('48','ru','ru.png','0'), ('49','sk','sk.png','0'), ('50','sl','sl.png','0'), ('51','so','so.png','0'), ('52','sq','sq.png','0'), ('53','sr','sr.png','0'), ('54','sv','sv.png','0'), ('55','ta','ta.png','0'), ('56','th','th.png','0'), ('57','tr','tr.png','0'), ('58','uk','uk.png','0'), ('59','ur','ur.png','0'), ('60','uz','uz.png','0'), ('61','vi','vi.png','0'), ('62','yi','yi.png','0'), ('63','zh-hans','zh.png','0'), ('64','zh-hant','zh.png','0'), ('65','zu','zu.png','0');
UNLOCK TABLES;

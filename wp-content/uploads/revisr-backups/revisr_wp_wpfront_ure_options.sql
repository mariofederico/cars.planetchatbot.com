DROP TABLE IF EXISTS `wp_wpfront_ure_options`;
CREATE TABLE `wp_wpfront_ure_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(250) DEFAULT NULL,
  `option_value` longtext,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
LOCK TABLES `wp_wpfront_ure_options` WRITE;
INSERT INTO `wp_wpfront_ure_options` VALUES ('1','wp_wpfront_ure_options-db-version','2.14.4'), ('2','attachment_capabilities_processed','1'), ('3','user_permission_capabilities_processed','1'), ('4','wp_wpfront_ure_login_redirect-db-version','2.14.4');
UNLOCK TABLES;

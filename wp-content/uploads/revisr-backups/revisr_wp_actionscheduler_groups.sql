DROP TABLE IF EXISTS `wp_actionscheduler_groups`;
CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `slug` (`slug`(191))
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_actionscheduler_groups` WRITE;
INSERT INTO `wp_actionscheduler_groups` VALUES ('1','action-scheduler-migration'), ('2','wc-admin-data'), ('3','wpforms'), ('4','wp_mail_smtp'), ('5','woocommerce-db-updates');
UNLOCK TABLES;

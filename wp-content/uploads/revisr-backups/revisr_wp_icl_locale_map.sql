DROP TABLE IF EXISTS `wp_icl_locale_map`;
CREATE TABLE `wp_icl_locale_map` (
  `code` varchar(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(35) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`code`,`locale`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_icl_locale_map` WRITE;
INSERT INTO `wp_icl_locale_map` VALUES ('en','en_US'), ('es','es_ES');
UNLOCK TABLES;

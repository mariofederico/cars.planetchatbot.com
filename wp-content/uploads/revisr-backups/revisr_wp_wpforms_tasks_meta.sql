DROP TABLE IF EXISTS `wp_wpforms_tasks_meta`;
CREATE TABLE `wp_wpforms_tasks_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_wpforms_tasks_meta` WRITE;
INSERT INTO `wp_wpforms_tasks_meta` VALUES ('1','wpforms_process_entry_emails_meta_cleanup','Wzg2NDAwXQ==','2021-06-07 00:16:16'), ('2','wpforms_admin_addons_cache_update','W10=','2021-06-07 00:16:17'), ('3','wpforms_admin_notifications_update','W10=','2021-06-07 00:16:26'), ('4','wpforms_admin_notifications_update','W10=','2021-06-08 14:55:47'), ('5','wpforms_admin_notifications_update','W10=','2021-06-09 15:19:18'), ('6','wpforms_admin_notifications_update','W10=','2021-06-10 17:30:49'), ('7','wpforms_admin_notifications_update','W10=','2021-06-15 02:32:47'), ('8','wpforms_admin_notifications_update','W10=','2021-06-16 12:28:07'), ('9','wpforms_admin_notifications_update','W10=','2021-06-17 15:30:02'), ('10','wpforms_admin_notifications_update','W10=','2021-06-18 16:02:10'), ('11','wpforms_admin_notifications_update','W10=','2021-06-24 17:54:29'), ('12','wpforms_admin_notifications_update','W10=','2021-06-29 18:15:58'), ('13','wpforms_admin_notifications_update','W10=','2021-06-30 18:49:39'), ('14','wpforms_admin_notifications_update','W10=','2021-07-01 22:33:53'), ('15','wpforms_admin_notifications_update','W10=','2021-07-03 01:24:12'), ('16','wpforms_admin_notifications_update','W10=','2021-07-05 16:24:30'), ('17','wpforms_admin_notifications_update','W10=','2021-07-07 01:03:29'), ('18','wpforms_admin_notifications_update','W10=','2021-07-07 01:04:33');
UNLOCK TABLES;

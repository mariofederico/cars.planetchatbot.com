DROP TABLE IF EXISTS `wp_wpfront_ure_login_redirect`;
CREATE TABLE `wp_wpfront_ure_login_redirect` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(250) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `url` varchar(2000) DEFAULT NULL,
  `deny_wpadmin` tinyint(1) DEFAULT NULL,
  `disable_toolbar` tinyint(1) DEFAULT NULL,
  `logout_url` varchar(2000) NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

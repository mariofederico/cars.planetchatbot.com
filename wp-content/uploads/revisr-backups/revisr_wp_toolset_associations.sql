DROP TABLE IF EXISTS `wp_toolset_associations`;
CREATE TABLE `wp_toolset_associations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `relationship_id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned NOT NULL,
  `child_id` bigint(20) unsigned NOT NULL,
  `intermediary_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relationship_id` (`relationship_id`),
  KEY `parent_id` (`parent_id`,`relationship_id`),
  KEY `child_id` (`child_id`,`relationship_id`),
  KEY `intermediary_id` (`intermediary_id`,`relationship_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

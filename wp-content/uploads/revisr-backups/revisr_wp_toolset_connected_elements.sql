DROP TABLE IF EXISTS `wp_toolset_connected_elements`;
CREATE TABLE `wp_toolset_connected_elements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` bigint(20) unsigned NOT NULL,
  `domain` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `wpml_trid` bigint(20) unsigned DEFAULT NULL,
  `lang_code` varchar(7) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `group_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `element` (`domain`,`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

DROP TABLE IF EXISTS `wp_icl_translation_downloads`;
CREATE TABLE `wp_icl_translation_downloads` (
  `editor_job_id` bigint(20) unsigned NOT NULL,
  `download_url` varchar(2000) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lock_timestamp` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`editor_job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

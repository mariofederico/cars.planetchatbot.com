DROP TABLE IF EXISTS `wp_icl_translate_job`;
CREATE TABLE `wp_icl_translate_job` (
  `job_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) unsigned NOT NULL,
  `translator_id` int(10) unsigned NOT NULL,
  `translated` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `manager_id` int(10) unsigned NOT NULL,
  `revision` int(10) unsigned DEFAULT NULL,
  `title` varchar(160) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `deadline_date` datetime DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  `editor` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `editor_job_id` bigint(20) unsigned DEFAULT NULL,
  `edit_timestamp` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  KEY `rid` (`rid`,`translator_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_icl_translate_job` WRITE;
INSERT INTO `wp_icl_translate_job` VALUES ('1','1','7','1','7','','Inicio','0000-00-00 00:00:00','2021-08-26 17:54:52','wp','12211325',''), ('2','9','7','1','7','1','2020 Bugatti Chiron Super Sport','0000-00-00 00:00:00','2021-08-29 16:14:02','ate','12269128',''), ('3','16','7','1','7','','My Dealer Account','0000-00-00 00:00:00','2021-08-27 22:03:56','ate','12244301',''), ('4','9','7','1','7','','2020 Bugatti Chiron Super Sport','0000-00-00 00:00:00','2021-08-31 14:58:47','ate','12325755',''), ('5','36','7','1','7','','2021 Alfa Romeo Stelvio','0000-00-00 00:00:00','2021-08-31 16:38:36','wp','12322683',''), ('6','37','7','1','7','','2010 Lamboghini Murcielago','0000-00-00 00:00:00','2021-08-31 17:54:57','wp','12323896',''), ('7','38','7','1','7','1','2020 Porsche Taycan','0000-00-00 00:00:00','','ate','12326044',''), ('8','38','7','1','7','','2020 Porsche Taycan','0000-00-00 00:00:00','2021-08-31 17:50:08','wp','12327573',''), ('9','39','7','1','7','','2021 Audi S8 TFSI','0000-00-00 00:00:00','2021-08-31 17:53:00','wp','12327652',''), ('10','40','7','1','7','','8 Nissan f-25','0000-00-00 00:00:00','2021-08-31 17:53:59','wp','12327672',''), ('11','41','7','1','7','','2021 BMW 840i Luxury Sports Coupe','0000-00-00 00:00:00','2021-08-31 17:55:52','ate','12327712',''), ('12','42','7','1','7','','2020 Ferrari 812 Superfast','0000-00-00 00:00:00','2021-08-31 17:56:28','wp','12327736',''), ('13','43','7','1','7','','2015 Bugatti Veyron','0000-00-00 00:00:00','2021-08-31 17:57:02','wp','12327741',''), ('14','44','7','1','7','','2021 Land Rover Defender','0000-00-00 00:00:00','2021-08-31 17:57:33','wp','12327750',''), ('15','45','7','1','7','','2021 Porsche Cayenne','0000-00-00 00:00:00','2021-08-31 17:58:03','wp','12327769',''), ('16','46','7','1','7','','2021 Mercedes Benz S500','0000-00-00 00:00:00','2021-08-31 17:59:41','wp','12327778',''), ('17','47','7','1','7','','2021 BMW Serie 4','0000-00-00 00:00:00','2021-08-31 17:58:50','wp','12327791',''), ('18','48','7','1','7','','2021 Lexus RX','0000-00-00 00:00:00','2021-08-31 17:59:13','wp','12327801',''), ('19','49','7','1','7','','2020 Porsche 911','0000-00-00 00:00:00','2021-08-31 17:59:42','wp','12327815',''), ('20','50','7','1','7','','Ferrari F40','0000-00-00 00:00:00','2021-08-31 18:00:13','wp','12327825',''), ('21','51','7','1','7','','2016 Chevrolet Silverado 1500','0000-00-00 00:00:00','2021-08-31 18:00:33','wp','12327832',''), ('22','52','7','1','7','','2012 Buick Enclave Leather','0000-00-00 00:00:00','2021-08-31 18:00:57','wp','12327841',''), ('23','53','7','1','7','','2017 Chevrolet Camaro 1SS','0000-00-00 00:00:00','2021-08-31 18:01:20','ate','12327848',''), ('24','54','7','1','7','','2015 Chevrolet Cruze 1LT Auto','0000-00-00 00:00:00','2021-08-31 18:01:29','wp','12327853',''), ('25','55','7','1','7','','2016 Chevrolet Sonic LT Auto','0000-00-00 00:00:00','2021-08-31 18:01:56','wp','12327868','');
UNLOCK TABLES;

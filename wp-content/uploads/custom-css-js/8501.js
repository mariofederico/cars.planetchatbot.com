<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
;
$=jQuery.noConflict();

jQuery(document).ready(function( $ ){
    
  if ( $(".text-left").length > 0 ) {
  // Si estoy en una pagina de Dealer
	
    $("#sort_agente-de-vehiculos + .nice-select.select-sort-filters.cd-select-box").hide();
    $("#sort_dealer + .nice-select.select-sort-filters.cd-select-box").hide();
    
  }	
  
  $("td.mileage").text("Millaje");
  $("nav.desktopTopFixed").css("height", "60px");
  $("div.menu-list-items").css("height", "60px");
  $("div.menu-inner").css("height", "60px");
  $(document).on("scroll", function(){
    $("div.menu-inner").css("height", "60px");
    $("nav.desktopTopFixed").css("height", "60px");
  	$("div.menu-list-items").css("height", "60px");
  	$("div.menu-inner").css("height", "60px"); 
  });
  

});


</script>
<!-- end Simple Custom CSS and JS -->

<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">


If you are using the jQuery library, then don't forget to wrap your code inside jQuery.ready() as follows:

jQuery(document).ready(function( $ ){
    // Your code in here
  
	var logo = document.querySelector('.menu-logo ul li a');
  	console.log(logo);  
  
});

--

</script>
<!-- end Simple Custom CSS and JS -->

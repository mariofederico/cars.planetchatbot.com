<?php
/**
 * Functions register the required plugins with TGMPA.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package CarDealer
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
get_template_part( 'includes/tgm-plugin-activation/core/class', 'tgm-plugin-activation' );

if ( ! function_exists( 'cardealer_tgmpa_plugin_list' ) ) {
	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	function cardealer_tgmpa_plugin_list() {
		/* required plugins */
		$plugins = array(
			array(
				'name'               => esc_html__( 'Car Dealer - Helper Library', 'cardealer' ),
				'slug'               => 'cardealer-helper-library',
				'source'             => cardealer_tgmpa_plugin_path( 'cardealer-helper-library-1.4.0.zip' ),
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => '',
				'version'            => '1.4.0',
				'checked_in_wizard'  => true,
			),
			array(
				'name'               => esc_html__( 'Visual Composer', 'cardealer' ),
				'slug'               => 'js_composer',
				'source'             => cardealer_tgmpa_plugin_path( 'js_composer-6.6.0.zip' ),
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => 'https://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431',
				'version'            => '6.6.0',
				'checked_in_wizard'  => true,
			),
			array(
				'name'               => esc_html__( 'Slider Revolution', 'cardealer' ),
				'slug'               => 'revslider',
				'source'             => cardealer_tgmpa_plugin_path( 'revslider-6.3.9.zip' ),
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => 'https://codecanyon.net/item/slider-revolution-responsive-wordpress-plugin/2751380',
				'version'            => '6.3.9',
				'checked_in_wizard'  => true,
			),
			array(
				'name'              => esc_html__( 'Redux Framework', 'cardealer' ),
				'slug'              => 'redux-framework',
				'required'          => true,
				'details_url'       => 'https://wordpress.org/plugins/redux-framework/',
				'checked_in_wizard' => true,
			),
			array(
				'name'               => esc_html__( 'Advanced Custom Fields PRO', 'cardealer' ),
				'slug'               => 'advanced-custom-fields-pro',
				'source'             => cardealer_tgmpa_plugin_path( 'advanced-custom-fields-pro-5.9.5.zip' ),
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => 'https://www.advancedcustomfields.com/pro/',
				'version'            => '5.9.5',
				'checked_in_wizard'  => true,
			),
			array(
				'name'              => esc_html__( 'Breadcrumb NavXT', 'cardealer' ),
				'slug'              => 'breadcrumb-navxt',
				'required'          => true,
				'details_url'       => 'https://wordpress.org/plugins/breadcrumb-navxt/',
				'checked_in_wizard' => true,
			),
			array(
				'name'              => esc_html__( 'Contact Form 7', 'cardealer' ),
				'slug'              => 'contact-form-7',
				'required'          => true,
				'details_url'       => 'https://wordpress.org/plugins/contact-form-7/',
				'checked_in_wizard' => true,
			),
		);

		/* recommended plugins */
		$recommended_plugins = array(
			array(
				'name'               => esc_html__( 'Car Dealer - Fronted Submission', 'cardealer' ),
				'slug'               => 'cardealer-front-submission',
				'source'             => cardealer_tgmpa_plugin_path( 'cardealer-front-submission-1.3.0.zip' ),
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => '',
				'version'            => '1.3.0',
				'checked_in_wizard'  => false,
			),
			array(
				'name'               => esc_html__( 'Subscriptio', 'cardealer' ),
				'slug'               => 'subscriptio',
				'source'             => cardealer_tgmpa_plugin_path( 'subscriptio-3.0.6.zip' ),
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => 'https://codecanyon.net/item/subscriptio-woocommerce-subscriptions/8754068',
				'version'            => '3.0.6',
				'checked_in_wizard'  => false,
			),
			array(
				'name'               => esc_html__( 'Car Dealer - VIN Import', 'cardealer' ),
				'slug'               => 'cardealer-vin-import',
				'source'             => cardealer_tgmpa_plugin_path( 'cardealer-vin-import-1.0.3.zip' ),
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => '',
				'version'            => '1.0.3',
				'checked_in_wizard'  => false,
			),
			array(
				'name'               => esc_html__( 'Car Dealer - VinQuery Import', 'cardealer' ),
				'slug'               => 'cardealer-vinquery-import',
				'source'             => cardealer_tgmpa_plugin_path( 'cardealer-vinquery-import-1.3.0.zip' ),
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'details_url'        => '',
				'version'            => '1.3.0',
				'checked_in_wizard'  => false,
			),
			array(
				'name'              => esc_html__( 'WooCommerce', 'cardealer' ),
				'slug'              => 'woocommerce',
				'required'          => false,
				'details_url'       => 'https://wordpress.org/plugins/woocommerce/',
				'checked_in_wizard' => false,
			),
			array(
				'name'              => esc_html__( 'Max Mega Menu', 'cardealer' ),
				'slug'              => 'megamenu',
				'required'          => false,
				'details_url'       => 'https://wordpress.org/plugins/megamenu/',
				'checked_in_wizard' => false,
			),
			array(
				'name'              => esc_html__( 'MailChimp for WordPress', 'cardealer' ),
				'slug'              => 'mailchimp-for-wp',
				'required'          => false,
				'details_url'       => 'https://wordpress.org/plugins/mailchimp-for-wp/',
				'checked_in_wizard' => false,
			),
			array(
				'name'               => esc_html__( 'Envato Market', 'cardealer' ),
				'slug'               => 'envato-market',
				'source'             => cardealer_tgmpa_plugin_path( 'envato-market-2.0.3.zip' ),
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'version'            => '2.0.3',
				'details_url'        => 'https://envato.com/market-plugin/',
				'checked_in_wizard'  => false,
			),
		);

		if ( ! isset( $_GET['step'] ) || 'default_plugins' !== $_GET['step'] ) {
			/* remove recommended plugins from installation wizard */
			$plugins = array_merge( $plugins, $recommended_plugins );
		}
		return apply_filters( 'tgmpa_plugin_list', $plugins );
	}
}

add_action( 'tgmpa_register', 'cardealer_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function cardealer_register_required_plugins() {
	if ( ! cardealer_is_activated() ) {
		return;
	}

	$plugins  = cardealer_tgmpa_plugin_list();
	$tgmpa_id = 'cardealer_recommended_plugins';

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => $tgmpa_id,           // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                  // Default absolute path to bundled plugins.
		'menu'         => 'theme-plugins',     // Menu slug.
		'parent_slug'  => 'themes.php',        // Parent menu slug.
		'capability'   => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                // Show admin notices or not.
		'dismissable'  => true,                // If false, a user cannot dismiss the nag message.
		'is_automatic' => false,               // Automatically activate plugins after installation or not.
	);
	tgmpa( $plugins, $config );
}

if ( ! function_exists( 'cardealer_tgmpa_setup_status' ) ) {
	/**
	 * Cardealer_tgmpa_setup_status()
	 * Returns plugin activation status
	 */
	function cardealer_tgmpa_setup_status() {

		$pluginy = cardealer_tgmpa_plugins_data();

		$cardealer_tgmpa_plugins_data_all = $pluginy['all'];
		foreach ( $cardealer_tgmpa_plugins_data_all as $cardealer_tgmpa_plugins_data_k => $cardealer_tgmpa_plugins_data_v ) {
			if ( ! $cardealer_tgmpa_plugins_data_v['required'] ) {
				unset( $cardealer_tgmpa_plugins_data_all[ $cardealer_tgmpa_plugins_data_k ] );
			}
		}

		if ( count( $cardealer_tgmpa_plugins_data_all ) > 0 ) {
			return false;
		} else {
			return true;
		}
	}
}

if ( ! function_exists( 'cardealer_tgmpa_plugins_data' ) ) {
	/**
	 * Cardealer_tgmpa_plugins_data()
	 * Returns plugin activation list
	 */
	function cardealer_tgmpa_plugins_data() {
		$plugins = cardealer_tgmpa_plugin_list();

		$tgmpax = call_user_func( array( get_class( $GLOBALS['tgmpa'] ), 'get_instance' ) );
		foreach ( $plugins as $plugin ) {
			call_user_func( array( $tgmpax, 'register' ), $plugin );
		}
		$pluginx = $tgmpax->plugins;

		$pluginy = array(
			'all'      => array(), // Meaning: all plugins which still have open actions.
			'install'  => array(),
			'update'   => array(),
			'activate' => array(),
		);

		foreach ( $tgmpax->plugins as $slug => $plugin ) {
			if ( $tgmpax->is_plugin_active( $slug ) && false === $tgmpax->does_plugin_have_update( $slug ) ) {
				// No need to display plugins if they are installed, up-to-date and active.
				continue;
			} else {
				$pluginy['all'][ $slug ] = $plugin;

				if ( ! $tgmpax->is_plugin_installed( $slug ) ) {
					$pluginy['install'][ $slug ] = $plugin;
				} else {
					if ( false !== $tgmpax->does_plugin_have_update( $slug ) ) {
						$pluginy['update'][ $slug ] = $plugin;
					}

					if ( $tgmpax->can_plugin_activate( $slug ) ) {
						$pluginy['activate'][ $slug ] = $plugin;
					}
				}
			}
		}
		return $pluginy;
	}
}

add_action( 'admin_head', 'cardealer_set_default_cdhl_plugin_version' );
if ( ! function_exists( 'cardealer_set_default_cdhl_plugin_version' ) ) {
	/**
	 * Function for update Car Dealer Helper Plugin
	 * Make entry in database for fresh installation so It will compare and do not ask for update
	 */
	function cardealer_set_default_cdhl_plugin_version() {
		global $pagenow;

		/* return if not on themes.php */
		if ( 'themes.php' !== $pagenow ) {
			return;
		}

		$plugin = 'cardealer-helper-library';
		if ( get_option( 'cdhl_version' ) === false ) {

			$do_version_entry = false;

			/*
			 * Installing from TGMPA
			 */

			// @codingStandardsIgnoreStart
			/* Single installation */
			if ( ( isset( $_GET['tgmpa-install'] ) && 'install-plugin' === $_GET['tgmpa-install'] ) && ( isset( $_GET['plugin'] ) && $_GET['plugin'] === $plugin ) ) {
				$do_version_entry = true;
			} elseif ( ( isset( $_POST['action'] ) || isset( $_POST['action2'] ) ) && ( 'tgmpa-bulk-install' === $_POST['action'] || 'tgmpa-bulk-install' === $_POST['action2'] ) ) {
				/* Bulk installation */
				$plugins_to_install = isset( $_POST['plugin'] ) ? $_POST['plugin'] : '';
				if ( in_array( $plugin, $plugins_to_install ) ) {
					/* check if specified plugin is available in bulk install */
					$do_version_entry = true;
				}
			}
			// @codingStandardsIgnoreEnd

			// Perform default verion entry if cardealer-helper-library plugin is found.
			if ( true === $do_version_entry ) {
				update_option( 'cdhl_version', '0.0.0' );
			}
		}
	}
}


if ( ! function_exists( 'cardealer_tgmpa_plugin_path' ) ) {
	/**
	 * Make plugin source URL
	 *
	 * @see cardealer_tgmpa_plugin_path()
	 *
	 * @param string $plugin_name used for html.
	 */
	function cardealer_tgmpa_plugin_path( $plugin_name = '' ) {
		$purchase_token = cardealer_is_activated();
		/* bail early if no plugin name provided */
		if ( empty( $plugin_name ) ) {
			return '';
		}
		return add_query_arg(
			array(
				'plugin_name' => $plugin_name,
				'token'       => $purchase_token,
				'site_url'    => get_site_url(),
				'product_key' => PGS_PRODUCT_KEY,
			),
			trailingslashit( PGS_ENVATO_API ) . 'install-plugin'
		);
	}
}

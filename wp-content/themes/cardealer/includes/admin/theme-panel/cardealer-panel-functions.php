<?php
/**
 * Cardealer Panel function file
 *
 * @package Cardealer
 */

// Support page.
if ( ! function_exists( 'cardealer_get_support_data' ) ) {
	/**
	 * Cardealer get support data
	 */
	function cardealer_get_support_data() {
		cardealer_load_theme_template( 'support' );
	}
}

// Registration page.
if ( ! function_exists( 'cardealer_display_theme_page' ) ) {
	/**
	 * Cardealer display theme page
	 */
	function cardealer_display_theme_page() {
		cardealer_load_theme_template( 'theme_token' );
	}
}

// Plugin Page.
if ( ! function_exists( 'cardealer_get_plugin_data' ) ) {
	/**
	 * Cardealer get plugin data
	 */
	function cardealer_get_plugin_data() {
		cardealer_load_theme_template( 'plugins' );
	}
}
// System Status.
if ( ! function_exists( 'cardealer_get_system_status' ) ) {
	/**
	 * Cardealer get system status
	 */
	function cardealer_get_system_status() {
		cardealer_load_theme_template( 'system_status' );
	}
}
// Cardealer Ratings.
if ( ! function_exists( 'cardealer_get_ratings_page' ) ) {
	/**
	 * Cardealer get ratings page
	 */
	function cardealer_get_ratings_page() {
		cardealer_load_theme_template( 'ratings' );
	}
}
/*Theme info*/
if ( ! function_exists( 'cardealer_get_theme_info' ) ) {
	/**
	 * Cardealer get theme info
	 */
	function cardealer_get_theme_info() {
		$theme      = wp_get_theme();
		$theme_name = $theme->get( 'Name' );
		$theme_v    = $theme->get( 'Version' );
		$theme_info = array(
			'name' => $theme_name,
			'slug' => sanitize_file_name( strtolower( $theme_name ) ),
			'v'    => $theme_v,
		);
		return $theme_info;
	}
}
if ( ! function_exists( 'cardealer_load_theme_template' ) ) {
	/**
	 * Cardealer load theme template
	 *
	 * @see cardealer_load_theme_template()
	 *
	 * @param string $path is string variable.
	 */
	function cardealer_load_theme_template( $path ) {
		$located = locate_template( 'templates/' . $path . '.php' );
		if ( $located ) {
			load_template( $located );
		} else {
			load_template( dirname( __FILE__ ) . '/templates/' . $path . '.php' );
		}
	}
}

if ( ! function_exists( 'cardealer_get_theme_support_url' ) ) {
	/**
	 * Cardealer get theme support url
	 */
	function cardealer_get_theme_support_url() {
		return esc_url( 'https://potezasupport.ticksy.com/submit/#100010500' );
	}
}
if ( ! function_exists( 'cardealer_get_theme_doc_url' ) ) {
	/**
	 * Cardealer get theme doc url
	 */
	function cardealer_get_theme_doc_url() {
		return esc_url( 'http://docs.potenzaglobalsolutions.com/docs/cardealer' );
	}
}
if ( ! function_exists( 'cardealer_get_theme_video_url' ) ) {
	/**
	 * Cardealer get theme video url
	 */
	function cardealer_get_theme_video_url() {
		return esc_url( 'http://docs.potenzaglobalsolutions.com/docs/cardealer/#videos' );
	}
}
if ( ! function_exists( 'cardealer_get_cardealer_tabs' ) ) {
	/**
	 * Cardealer get cardealer tabs
	 *
	 * @see cardealer_get_cardealer_tabs()
	 *
	 * @param string $screen is string variable.
	 */
	function cardealer_get_cardealer_tabs( $screen = 'support' ) {
		$theme      = cardealer_get_theme_info();
		$theme_name = $theme['name'];
		?>
		<div class="clearfix">
			<div class="cardealer-about-text-wrap">
				<h1>
					<?php
					/* translators: %s: Theme name */
					printf( esc_html__( 'Welcome to %s', 'cardealer' ), esc_html( $theme_name ) );
					?>
				</h1>
				<div class="cardealer-about-text about-text">
					<?php add_thickbox(); ?>
					<div class="cardealer_theme_info cardealer-welcome">
						<div class="welcome-left cardealer-welcome-badge <?php echo esc_attr( cardealer_welcome_logo() ? 'cardealer-welcome-badge-with-logo' : 'cardealer-welcome-badge-without-logo' ); ?>">
							<div class="wp-badge">
								<img src="<?php echo esc_url( CARDEALER_URL . '/images/theme-panel/admin-welcome-logo.png' ); ?>" height="100" width="100" />
							</div>
							<div class="cardealer-welcome-badge-version">
								<?php
								/* translators: %s: Theme name */
								echo sprintf( esc_html__( 'Version %s', 'cardealer' ), esc_html( $theme['v'] ) );
								?>
							</div>
						</div>
						<div class="welcome-right">
							<?php printf( wp_kses( __( '<strong>Car Dealer</strong> is now active and ready to use! <strong>Car Dealer</strong> is an elegant, clean, beautiful and best responsive WordPress theme. <strong>Car Dealer</strong> contains many usefull features and functionalities. And, it requires some plugins to be pre-installed to enable all inbuilt features and functionalities.', 'cardealer' ), array( 'strong' => array() ) ), esc_html( $theme_name ), esc_html( $theme_name ) ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if ( function_exists( 'cdhl_activate' ) && class_exists( 'redux' ) ) { ?>
			<h2 class="nav-tab-wrapper">
				<a href="<?php echo esc_url_raw( admin_url( 'admin.php?page=cardealer-panel' ) ); ?>"
				class="<?php echo ( 'support' === $screen ) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e( 'Support', 'cardealer' ); ?></a>
				<a href="<?php echo esc_url_raw( admin_url( 'admin.php?page=cardealer-plugins' ) ); ?>"
				class="<?php echo ( 'plugins' === $screen ) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e( 'Plugins', 'cardealer' ); ?></a>
				<a href="<?php echo esc_url_raw( admin_url( 'admin.php?page=cardealer&cd_section=sample_data' ) ); ?>"
				class="<?php echo ( 'demos' === $screen ) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e( 'Install Demos', 'cardealer' ); ?></a>
				<a href="<?php echo esc_url_raw( admin_url( 'admin.php?page=cardealer' ) ); ?>" class="nav-tab"><?php esc_html_e( 'Theme Options', 'cardealer' ); ?></a>
				<a href="<?php echo esc_url_raw( admin_url( 'admin.php?page=cardealer-system-status' ) ); ?>"
				class="<?php echo ( 'system-status' === $screen ) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e( 'System Status', 'cardealer' ); ?></a>
				<a href="<?php echo esc_url_raw( admin_url( 'admin.php?page=cardealer-ratings' ) ); ?>"
				class="<?php echo ( 'ratings' === $screen ) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e( 'Ratings', 'cardealer' ); ?></a>
			</h2>
			<?php
		}
	}
}
if ( ! function_exists( 'cardealer_convert_memory' ) ) {
	/**
	 * Cardealer convert memory
	 *
	 * @see cardealer_convert_memory()
	 *
	 * @param string $size is string variable.
	 *
	 * @return int
	 */
	function cardealer_convert_memory( $size ) {
		$l    = substr( $size, -1 );
		$ret  = substr( $size, 0, -1 );
		$byte = 1024;

		switch ( strtoupper( $l ) ) {
			case 'P':
				$ret *= $byte;
				// No break.
			case 'T':
				$ret *= $byte;
				// No break.
			case 'G':
				$ret *= $byte;
				// No break.
			case 'M':
				$ret *= $byte;
				// No break.
			case 'K':
				$ret *= $byte;
				// No break.
		}
		return $ret;
	}
}
?>

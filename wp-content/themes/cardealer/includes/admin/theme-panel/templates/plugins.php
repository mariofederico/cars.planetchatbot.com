<?php
/**
 * Do not allow directly accessing this file.
 *
 * @package Cardealer
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
} ?>
<div class="wrap cardealer-admin-theme-page cardealer-admin-wrap  cardealer-admin-plugins-screen">
<?php
	cardealer_get_cardealer_tabs( 'plugins' );
	$tgm_page_plugins = new TGM_Plugin_Activation();
	$tgm_page_plugins->install_plugins_page();
?>
</div>

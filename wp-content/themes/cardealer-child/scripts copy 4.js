
/*=============================================
SCRIPTS PERSONALIDZADOS
=============================================*/
;
$ = jQuery.noConflict();

jQuery(document).ready(function( $ ){


	/*****************************************************
	SCRIPT PARA NO MOSTRAR LOS FILTROS DEALER Y VENDEDORES
	******************************************************/

	if ( $(".text-left").length > 0 ) {
  	// Si estoy en una pagina de Dealer
	
    $("#sort_agente-de-vehiculos + .nice-select.select-sort-filters.cd-select-box").hide();
    $("#sort_dealer + .nice-select.select-sort-filters.cd-select-box").hide();

    //Seleccionar el Dealer actual en el filtro

    /*no funciono usar javascript para seleccionar el filtro*/

    /*$("#sort_dealer + .nice-select.select-sort-filters.cd-select-box").click();
    $('#cdhl-vehicle-filters-1 > div.listing_sort > div > div.nice-select.select-sort-filters.cd-select-box.open > ul > li:nth-child(2)').click();

    $('#cdhl-vehicle-filters-1 > div.listing_sort > div > div:nth-child(4)').hide();

    $('#stripe-item-dealer > a').hide();

    $("#sort_agente-de-vehiculos + .nice-select.select-sort-filters.cd-select-box").hide();

    $('#cdhl-vehicle-filters-1 > div.listing_sort > div > div:nth-child(4)').hide();*/
    
  	}
	

	/*****************************************************
	EMAILS Y TELEFONOS EN LAS DESCRIPCIONES
	******************************************************/

	/*TELEFONO*/
	
	var telefono = $("li.telefono > strong.text-right").text();

	$("li.telefono > strong").replaceWith("<a>");

	$("li.telefono > a").text(telefono);

	$("li.telefono > a").attr("href","tel:+58"+telefono);

	$("li.telefono > a").attr("class","text-right");

	$("li.telefono > a").css("display","table-cell");

	
	/*EMAIL*/

	var email = $("li.email > strong.text-right").text();

	$("li.email > strong").replaceWith("<a>");

	$("li.email > a").text(email);

	$("li.email > a").attr("href","mailto:"+email);

	$("li.email > a").attr("class","text-right");

	$("li.email > a").css("display","table-cell");


	/*****************************************************
	SCRIPT PARA LINK DE PÁGINA DEL DEALER EN DESCRIPCIONES
	******************************************************/

	/*DEALER*/

	var dealer = $("li.dealer > strong.text-right").text();

	var link_dealer = $("div.dealer_des a").attr('href');

	$("li.dealer > strong").replaceWith("<a>");

	$("li.dealer > a").text(dealer);

	$("li.dealer > a").attr("href",link_dealer);

	$("li.dealer > a").attr("class","text-right");

	$("li.dealer > a").css("display","table-cell");

	$("div.dealer_des").hide();
    
  	/*****************************************************
	SCRIPT PARA MODIFICAR PÁGINA DEL DEALER
	******************************************************/

	$('section.inner-intro').hide();







}); 

/*=============================================
SCRIPTS PERSONALIDZADOS
=============================================*/
;
$ = jQuery.noConflict();

jQuery(document).ready(function( $ ){


	/*****************************************************
	ENCABEZADO INTERNO DE LAS PAGINAS
	******************************************************/


	$('section.inner-intro').hide();


	/*****************************************************
	SCRIPT PARA NO MOSTRAR LOS FILTROS DEALER Y VENDEDORES
	******************************************************/

	if ( $(".nombre-dealer").length > 0 ) {
  	// Si estoy en una pagina de Dealer
	
    $("#sort_agente-de-vehiculos + .nice-select.select-sort-filters.cd-select-box").hide();
    $("#sort_dealer + .nice-select.select-sort-filters.cd-select-box").hide();

    //Seleccionar el Dealer actual en el filtro

    /*no funciono usar javascript para seleccionar el filtro*/

    /*$("#sort_dealer + .nice-select.select-sort-filters.cd-select-box").click();
    $('#cdhl-vehicle-filters-1 > div.listing_sort > div > div.nice-select.select-sort-filters.cd-select-box.open > ul > li:nth-child(2)').click();

    $('#cdhl-vehicle-filters-1 > div.listing_sort > div > div:nth-child(4)').hide();

    $('#stripe-item-dealer > a').hide();

    $("#sort_agente-de-vehiculos + .nice-select.select-sort-filters.cd-select-box").hide();

    $('#cdhl-vehicle-filters-1 > div.listing_sort > div > div:nth-child(4)').hide();*/
    
  	}
	

  	/*****************************************************
	EMAILS Y TELEFONOS EN LAS DESCRIPCIONES BOTON VER
	******************************************************/

	/*TELEFONO*/

	var telefono = $("li.telefono > strong.text-right").text();

	$("li.telefono > strong").replaceWith("<a>");

	$("li.telefono > a").text("Ver");

	$("li.telefono > a").attr("href","#");

	$("li.telefono > a").attr("class","text-right");

	$("li.telefono > a").css("display","table-cell");
	
	$("li.telefono > a").click(function(){

		/*$("li.telefono > strong").replaceWith("<a>");*/

		$("li.telefono > a").text(telefono);

		$("li.telefono > a").on( "click", function(){

		$("li.telefono > a").attr("href","tel:+58"+telefono);

	});

		/*$("li.telefono > a").attr("class","text-right");

		$("li.telefono > a").css("display","table-cell");*/

	});

	/*EMAIL*/

		var email = $("li.email > strong.text-right").text();

	$("li.email > strong").replaceWith("<a>");

	$("li.email > a").text("Ver");

	$("li.email > a").attr("href","#");

	$("li.email > a").attr("class","text-right");

	$("li.email > a").css("display","table-cell");
	
	$("li.email > a").click(function(){

		/*$("li.telefono > strong").replaceWith("<a>");*/

		$("li.email > a").text(email);

		$("li.email > a").on( "click", function(){

		$("li.email > a").attr("href","mailto:"+email);

	});

		/*$("li.telefono > a").attr("class","text-right");

		$("li.telefono > a").css("display","table-cell");*/

	});

	/*****************************************************
	SCRIPT PARA LINK DE PÁGINA DEL DEALER EN DESCRIPCIONES
	******************************************************/

	/*DEALER*/

	/*$("div.dealer_des").hide();*/

	var dealer = $("li.dealer > strong.text-right").text();

	var link_dealer = $("div.dealer_des a").attr('href');

	$("li.dealer > strong").replaceWith("<a>");

	$("li.dealer > a").text(dealer);

	$("li.dealer > a").attr("href",link_dealer);

	$("li.dealer > a").attr("class","text-right");

	$("li.dealer > a").css("display","table-cell");

	
    
  	/*****************************************************
	SCRIPT PARA MODIFICAR PÁGINA DE CADA DEALER
	******************************************************/

	

	/*COLOR PARA LA SECCION DE CADA DEALER*/

    if ( $(".nombre-dealer").length > 0 ) {

    	

    	$('div.col-sm-12.cd-content').css("background-color","#3F5760");
    	$('.product-listing.lazyload .col-sm-12 + .col-sm-12').css("padding-top","53px");
    	/*$('.product-listing.lazyload .cars-top-filters-box-right').css("padding","0px");*/
    	$('.cars-top-filters-box-right').css("padding-top","70px")/*55*/

    };

    /*BOTONES PARA DIRECCION, EMAIL Y TELEFONO*/

    $('a.link-modal').css("color","white")

    $('button#email').click(function(){

    	$('button.email-dealer').show();
    	$(this).hide();


    	if ($(Window).width() <= 320) {
            
    		$('div.email').css("padding-left","29px");
    		$('div.email').css("padding-right","15px");
    		$('div.telefono').css("padding-left","15px");
    		$('div.telefono').css("margin-right","14px");
    		$('div.direccion').css("padding-left","0px");

        };
    	

    });

    $('button#telefono').click(function(){

    	$('button.tel-dealer').show();
    	$(this).hide();
    	

    });


	/*****************************************************
	MENU TAMAÑO DE MENU
	******************************************************/

	$('div.menu-inner').css("height","55px");


}); 
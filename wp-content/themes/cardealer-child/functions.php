<?php
/**
 * Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package CarDealer
 */

/*
 * If your child theme has more than one .css file (eg. ie.css, style.css, main.css) then
 * you will have to make sure to maintain all of the parent theme dependencies.
 *
 * Make sure you're using the correct handle for loading the parent theme's styles.
 * Failure to use the proper tag will result in a CSS file needlessly being loaded twice.
 * This will usually not affect the site appearance, but it's inefficient and extends your page's loading time.
 *
 * @link https://codex.wordpress.org/Child_Themes
 */

/*MEJORANDO REGISTROS PARA EL CHILD ORIGINAL*/

if(!function_exists('cardealer_child_enqueue_styles')):

	function cardealer_child_enqueue_styles() {// phpcs:ignore WordPress.WhiteSpace.ControlStructureSpacing.NoSpaceAfterOpenParenthesis

		wp_enqueue_style( 'cardealer-main', get_parent_theme_file_uri( '/css/style.css' ) );

		if ( is_rtl() ) {
		wp_enqueue_style( 'rtl-style', get_parent_theme_file_uri( '/rtl.css' ) );
		}

		wp_enqueue_style(
		'cardealer-child-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'cardealer-main' ),
		wp_get_theme()->get( 'Version' )
		);

	}

endif;

add_action( 'wp_enqueue_scripts', 'cardealer_child_enqueue_styles', 11 );

if(!function_exists('cardealer_custom_scripts')):

	function cardealer_custom_scripts(){

		//REGISTRANDO ARCHIVO DE SCRIPTS PERSONALIZADOS

		wp_register_script('scripts', get_template_directory_uri().'-child/scripts.js', array('jquery'),'1.0.0',true);

		//MANDANDO A LA COLA EL ARCHIVO SCRIPTS

		wp_enqueue_script('jquery');

		wp_enqueue_script('scripts');

	}

endif;

add_action( 'wp_enqueue_scripts', 'cardealer_custom_scripts', 11 );



//Modificar kilometraje a Millaje
function modify_taxonomy_car_mileage() {
 
    //Recupero la taxonomía
    $taxonomy_args = get_taxonomy( 'car_mileage' );
 
     // Definimos un array para las Etiquetas de la taxonomía
    $taxonomy_args ->labels = array(
        'name' => __( 'Millaje' ),
        'singular_name' => __( 'Millaje' ),
        'search_items' =>  __( 'Buscar Millaje' ),
        'all_items' => __( 'Todos los Millajes' ),
        'parent_item' => __( 'Millaje padre' ),
        'parent_item_colon' => __( 'Millaje padre:' ),
        'edit_item' => __( 'Editar Millaje' ), 
        'update_item' => __( 'Actualizar Millaje' ),
        'add_new_item' => __( 'Agregar un nuevo Millaje' ),
        'menu_name' => __( 'Millaje' ),
        'item_list' => __('Millaje'),
        'item_list_navigation' => __('Millaje'),
        'Update_item_name' => __('Actualizar Millaje'),
    ); 	
    //volver a registrar la taxonomía con los cambios
    register_taxonomy( 'car_mileage', 'cars', (array)$taxonomy_args);
}
//Poner una prioridad alta, para que sobreescriba la taxonomía original
add_action( 'init', 'modify_taxonomy_car_mileage', 13 );


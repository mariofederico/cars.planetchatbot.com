
/*=============================================
SCRIPTS PERSONALIDZADOS
=============================================*/
;
$ = jQuery.noConflict();

jQuery(document).ready(function( $ ){


	/*****************************************************
	SCRIPT PARA NO MOSTRAR LOS FILTROS DEALER Y VENDEDORES
	******************************************************/

	/*if ( $(".text-left").length > 0 ) {
  	// Si estoy en una pagina de Dealer
	
    $("#sort_agente-de-vehiculos + .nice-select.select-sort-filters.cd-select-box").hide();
    $("#sort_dealer + .nice-select.select-sort-filters.cd-select-box").hide();
    
  	}*/
	

	/*****************************************************
	EMAILS Y TELEFONOS EN LAS DESCRIPCIONES
	******************************************************/
	
	var telefono = $("li.telefono > strong.text-right").text();

	$("li.telefono > strong").replaceWith("<a>");

	$("li.telefono > a").text(telefono);

	$("li.telefono > a").attr("href","tel:+58"+telefono);

	$("li.telefono > a").attr("class","text-right");

	$("li.telefono > a").style("text-align","right");

	$("li.telefono > a").style("display","table-cell");

}); 